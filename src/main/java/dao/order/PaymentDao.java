package dao.order;

import dao.Dao;
import model.order.Payment;

public class PaymentDao extends Dao<Payment>{

	public PaymentDao() {
		super(Payment.class);
	}

}
