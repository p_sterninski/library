package dao.order;

import dao.Dao;
import model.order.Order;

public class OrderDao extends Dao<Order> {

	public OrderDao() {
		super(Order.class);
	}

}
