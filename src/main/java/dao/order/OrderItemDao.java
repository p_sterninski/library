package dao.order;

import dao.Dao;
import model.order.OrderItem;

public class OrderItemDao extends Dao<OrderItem> {

	public OrderItemDao() {
		super(OrderItem.class);
	}

}
