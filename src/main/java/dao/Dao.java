package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

public abstract class Dao <T> {
	
	Class<T> objectClass; 
	
	
	public Dao(Class<T> objectClass) {
		this.objectClass = objectClass;
	}
	
	
	public void save(T object) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.save(object);
		
		session.getTransaction().commit();
	}
	
	
	
	public T findById(Long id) {
		T object = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		object = session.get(objectClass, id);
		
		session.getTransaction().commit();
		return object;
	}
	
	

	public void update(T object) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.update(object);
		
		session.getTransaction().commit();
	}
	
	
	
	public void delete(T object) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.delete(object);
		
		session.getTransaction().commit();
	}
	
	
	

	@SuppressWarnings("unchecked")
	public List<T> getAll(){
		List<T> list = new ArrayList<>();
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		//list = session.createCriteria(objectClass).list();
		list = session.createQuery("from " + objectClass.getSimpleName()).list();
		
		session.getTransaction().commit();
		
		return list;
		
	}
	
	
	
}
