package dao.product;

import dao.Dao;
import model.product.Product;

public class ProductDao extends Dao<Product> {

	public ProductDao() {
		super(Product.class);
	}


}
