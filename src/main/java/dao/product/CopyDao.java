package dao.product;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import dao.Dao;
import dao.HibernateUtil;
import model.product.Copy;

public class CopyDao extends Dao<Copy> {
	
	public CopyDao() {
		super(Copy.class);
	}


//	@SuppressWarnings("unchecked")
//	public List<Copy> findAll(){
//		List<Copy> listAll = new ArrayList<>();
//		
//		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//		session.beginTransaction();
//		
//		listAll = session.createQuery("from Copy").list();
//		session.getTransaction().commit();
//		
//		return listAll;
//	}
	
	
	public Copy findBySignature(String signature) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
//		@SuppressWarnings("deprecation")
//		Query<Copy> query = session.createQuery("from Copy where signature = :signatureFind");
//		query.setString("signatureFind", signature);
//		
//		Copy copy = (Copy)query.uniqueResult();
		
		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Copy> criteriaQuery = builder.createQuery(Copy.class);
        Root<Copy> root = criteriaQuery.from(Copy.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get("signature"), signature));
        Copy copy = session.createQuery(criteriaQuery).uniqueResult();
		
		session.getTransaction().commit();
		return copy;
	}
}
