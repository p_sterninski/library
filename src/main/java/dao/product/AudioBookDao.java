package dao.product;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import dao.HibernateUtil;
import model.product.AudioBook;

public class AudioBookDao {

	
	@SuppressWarnings("unchecked")
	public List<AudioBook> findAll(){
		List<AudioBook> listAll = new ArrayList<>();
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		listAll = session.createQuery("from AudioBook").list();
		session.getTransaction().commit();
		
		return listAll;
	}
	
	
	
}
