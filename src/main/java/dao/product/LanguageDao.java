package dao.product;

import dao.Dao;
import model.product.Language;

public class LanguageDao extends Dao<Language>{

	public LanguageDao(Class<Language> objectClass) {
		super(Language.class);
	}

}
