package dao.product;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import dao.HibernateUtil;
import model.product.PrintBook;

public class PrintBookDao {

	@SuppressWarnings("unchecked")
	public List<PrintBook> findAll(){
		List<PrintBook> listAll = new ArrayList<>();
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		listAll = session.createQuery("from PrintBook").list();
		session.getTransaction().commit();
		
		return listAll;
	}
}
