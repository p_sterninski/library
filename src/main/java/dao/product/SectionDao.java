package dao.product;

import dao.Dao;
import model.product.Section;

public class SectionDao extends Dao<Section>{

	public SectionDao() {
		super(Section.class);
	}

}
