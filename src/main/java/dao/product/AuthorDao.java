package dao.product;

import dao.Dao;
import model.product.Author;

public class AuthorDao extends Dao<Author>{

	public AuthorDao() {
		super(Author.class);
	}

}
