package dao.product;

import dao.Dao;
import model.product.Delivery;

public class DeliveryDao extends Dao<Delivery> {

	public DeliveryDao() {
		super(Delivery.class);
	}

}
