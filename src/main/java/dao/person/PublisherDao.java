package dao.person;

import dao.Dao;
import model.person.Publisher;

public class PublisherDao extends Dao<Publisher>{

	public PublisherDao() {
		super(Publisher.class);
	}

}
