package dao.person;

import dao.Dao;
import model.person.Vendor;

public class VendorDao extends Dao<Vendor>{

	public VendorDao(Class<Vendor> objectClass) {
		super(Vendor.class);
	}

}
