package dao.person;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import dao.Dao;
import dao.HibernateUtil;
import model.person.NaturalPerson;
import model.person.PersonStatus;

public class NaturalPersonDao extends Dao<NaturalPerson> {
	
	public NaturalPersonDao() {
		super(NaturalPerson.class);
	}
	
	
	
	
	public NaturalPerson findByCardNo(String cardNo) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
//		NaturalPerson client = (NaturalPerson) session.createCriteria(NaturalPerson.class)
//														.add(Restrictions.eq("cardNo", cardNo))
//														.uniqueResult();
		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<NaturalPerson> criteriaQuery = builder.createQuery(NaturalPerson.class);
        Root<NaturalPerson> root = criteriaQuery.from(NaturalPerson.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get("cardNo"), cardNo));
        NaturalPerson client =  session.createQuery(criteriaQuery).uniqueResult();
        
		session.getTransaction().commit();
		
		return client;
	}
	
	
	
	public List<NaturalPerson> getClients() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<NaturalPerson> criteriaQuery = builder.createQuery(NaturalPerson.class);
        Root<NaturalPerson> root = criteriaQuery.from(NaturalPerson.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.notEqual(root.get("personStatus"), PersonStatus.Employee));
        List<NaturalPerson> clients =  session.createQuery(criteriaQuery).getResultList();
        
		session.getTransaction().commit();
		
		return clients;
	}
	
	
	public List<NaturalPerson> getEmployees() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<NaturalPerson> criteriaQuery = builder.createQuery(NaturalPerson.class);
        Root<NaturalPerson> root = criteriaQuery.from(NaturalPerson.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.notEqual(root.get("personStatus"), PersonStatus.Client));
        List<NaturalPerson> employees =  session.createQuery(criteriaQuery).getResultList();
        
		session.getTransaction().commit();
		
		return employees;
	}
	
}
