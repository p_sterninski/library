package dao.person;

import dao.Dao;
import model.person.LegalPerson;

public class LegalPersonDao extends Dao<LegalPerson>{

	public LegalPersonDao() {
		super(LegalPerson.class);
	}

}
