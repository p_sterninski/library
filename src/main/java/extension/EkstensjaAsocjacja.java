package extension;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class EkstensjaAsocjacja extends Extension {
	
	private HashMap<String, HashMap<Object, EkstensjaAsocjacja>> powiazania = new HashMap<>();
	
	private static HashSet<EkstensjaAsocjacja> wszystkieCzesci = new HashSet<>();
	
	
//konstruktor
	public EkstensjaAsocjacja() {
		super();
	}
	
	
//dodajPowiazanie
    private void dodajPowiazanie(String nazwaRoli, String odwrotnaNazwaRoli, EkstensjaAsocjacja obiektDocelowy, Object kwalifikator, int licznik) {
        
    	HashMap<Object, EkstensjaAsocjacja> powiazaniaObiektu;
 
        if(licznik < 1) {
            return;
        }
 
        if(powiazania.containsKey(nazwaRoli)) {
            // Pobierz te powiazania
            powiazaniaObiektu = powiazania.get(nazwaRoli);
        }
        else {
            // Brak powiazan dla takiej roli ==> utworz
            powiazaniaObiektu = new HashMap<Object, EkstensjaAsocjacja>();
            powiazania.put(nazwaRoli, powiazaniaObiektu);
        }
 
        if(!powiazaniaObiektu.containsKey(kwalifikator)) {
            // Dodaj powiazanie dla tego obiektu
            powiazaniaObiektu.put(kwalifikator, obiektDocelowy);
            // Dodaj powiazanie zwrotne
            obiektDocelowy.dodajPowiazanie(odwrotnaNazwaRoli, nazwaRoli, this, this, licznik - 1);
        }
    }
    
    
    public void dodajPowiazanie(String nazwaRoli, String odwrotnaNazwaRoli, EkstensjaAsocjacja obiektDocelowy, Object kwalifikator) {
        dodajPowiazanie(nazwaRoli, odwrotnaNazwaRoli, obiektDocelowy, kwalifikator, 2);
    }
 
    public void dodajPowiazanie(String nazwaRoli, String odwrotnaNazwaRoli, EkstensjaAsocjacja obiektDocelowy) {
        dodajPowiazanie(nazwaRoli, odwrotnaNazwaRoli, obiektDocelowy, obiektDocelowy);
    }
    
    
//dodajCzesc
    public void dodajCzesc(String nazwaRoli, String odwrotnaNazwaRoli, EkstensjaAsocjacja obiektCzesc) throws Exception {
    	 
        // Sprawdz czy ta czesc juz gdzies nie wystepuje
        if(wszystkieCzesci.contains(obiektCzesc)) {
            throw new Exception("Ta czesc jest już powiazana z jakas caloscia!");
        }
 
        dodajPowiazanie(nazwaRoli, odwrotnaNazwaRoli, obiektCzesc);
 
        // Zapamietaj dodanie obiektu jako czesci
        wszystkieCzesci.add(obiektCzesc);
    }    
    
    
    
    public EkstensjaAsocjacja[] dajPowiazania(String nazwaRoli) throws Exception {
        
    	HashMap<Object, EkstensjaAsocjacja> powiazaniaObiektu;
 
        if(!powiazania.containsKey(nazwaRoli)) {
            // Brak powiazan dla tej roli
            throw new Exception("Brak powiazan dla roli: " + nazwaRoli);
        }
 
        powiazaniaObiektu = powiazania.get(nazwaRoli);
        return (EkstensjaAsocjacja[]) powiazaniaObiektu.values().toArray(new EkstensjaAsocjacja[0]);
    }
    
    
    public void wyswietlPowiazania(String nazwaRoli, PrintStream stream) throws Exception {
        HashMap<Object, EkstensjaAsocjacja> powiazaniaObiektu;
 
        if(!powiazania.containsKey(nazwaRoli)) {
            // Brak powiazan dla tej roli
            throw new Exception("Brak powiazan dla roli: " + nazwaRoli);
        }
 
        powiazaniaObiektu = powiazania.get(nazwaRoli);
        Collection col = powiazaniaObiektu.values();
        stream.println(this.getClass().getSimpleName() + " powiazania w roli " +    nazwaRoli + ":");
 
        for(Object obj : col) {
            stream.println("   " + obj);
        }
    }
    
    
    public EkstensjaAsocjacja dajPowiazanyObiekt(String nazwaRoli, Object kwalifikator) throws Exception {
        HashMap<Object, EkstensjaAsocjacja> powiazaniaObiektu;
 
        if(!powiazania.containsKey(nazwaRoli)) {
            // Brak powiazan dla tej roli
            throw new Exception("Brak powiazan dla roli: " + nazwaRoli);
        }
 
        powiazaniaObiektu = powiazania.get(nazwaRoli);
        if(!powiazaniaObiektu.containsKey(kwalifikator)) {
            // Brak powiazan dla tej roli
            throw new Exception("Brak powiazania dla kwalifikatora: " + kwalifikator);
        }
 
        return powiazaniaObiektu.get(kwalifikator);
    }
}
