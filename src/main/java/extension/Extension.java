package extension;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Extension 
			implements Serializable {

	private static HashMap<Class<? extends Extension>, List<Extension>> extensionMap = new HashMap<>();
	
	
	public Extension() {
		List<Extension> extensionList = null;
		Class<? extends Extension> instanceClass = this.getClass();
		
		if(extensionMap.containsKey(instanceClass)) {
			extensionList = extensionMap.get(instanceClass);
		} else {
			extensionList = new ArrayList<Extension>();
			extensionMap.put(instanceClass, extensionList);
		}
		
		extensionList.add(this);
	}
	
	
	
	public static void saveExtensionMap(ObjectOutputStream stream) 
			throws IOException {
		stream.writeObject(extensionMap);
	}
	
	
	
	public static void loadExtensionMap(ObjectInputStream stream) 
			throws IOException, ClassNotFoundException {
		extensionMap = (HashMap<Class<? extends Extension>, List<Extension>>) stream.readObject();
	}
	
	
	
	public static void showExtension(Class<? extends Extension> instanceClass) throws Exception {
		List<Extension> extensionList = null;
		
		if(extensionMap.containsKey(instanceClass)) {
			extensionList = extensionMap.get(instanceClass);
		} else {
			throw new Exception("Nieznana klasa " + instanceClass);
		}
		
		System.out.println("Klasa ekstensji " + instanceClass.getSimpleName());
		for(Extension object : extensionList) {
			System.out.println(object);
		}
	}
	
	
	
	public static int getAmountInExtension(Class<? extends Extension> instanceClass) {
		int amount = 0;
		
		if(extensionMap.containsKey(instanceClass)) {
			amount = extensionMap.get(instanceClass).size();			
		} 
		
		return amount;
	}
	
}
