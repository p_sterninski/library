package controller;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import dao.product.CopyDao;
import dao.product.ProductDao;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import model.person.NaturalPerson;
import model.product.Author;
import model.product.Copy;
import model.product.Product;



public class OrderItemChooseController {
	
	@FXML
	private TableView<Product> productTableView;

	@FXML
	private TableColumn<Product, String> titleColumn;

	@FXML
	private TableColumn<Product, Integer> editionColumn;

	@FXML
	private TableColumn<Product, Integer> releaseYearColumn;
	
	@FXML
	private TableColumn<Product, String> authorColumn;
	
	@FXML
	private TableColumn<Product, String> publisherColumn;

	@FXML
	private TableView<Copy> copyTableView;

	@FXML
	private TableColumn<Copy, String> signatureColumn;

	@FXML
	private TableColumn<Copy, String> copyStatusColumn;
	
	@FXML
	private TextField signatureTextField;

	@FXML
	private Label signatureLabel;

	@FXML
	private Label copyStatusLabel;
	
	@FXML
	private TableView<Copy> chosenCopyTableView;

	@FXML
	private TableColumn<Copy, String> chosenTitleColumn;

	@FXML
	private TableColumn<Copy, String> chosenSignatureColumn;
	
	
	private Copy chosenCopy;
	private NaturalPerson client;
	private RegisterOrderController registerOrderController;
	private Set<Copy> copyList = new HashSet<>();
	
	
	public void initialize() {
		
		//inicjalizacja kolumn dla productTableView
		getTitleColumn().setCellValueFactory(new PropertyValueFactory<>("title"));
		getEditionColumn().setCellValueFactory(new PropertyValueFactory<>("edition"));
		getReleaseYearColumn().setCellValueFactory(new PropertyValueFactory<>("releaseYear"));
		getAuthorColumn().setCellValueFactory(cellData -> {
			String authors = cellData.getValue()
									.getAuthorList()
									.stream()
									.map(Author::toString)
									.collect(Collectors.joining(", "));
			return new ReadOnlyStringWrapper(authors);
		});
		getPublisherColumn().setCellValueFactory(cellData -> {
			String publisherName = cellData.getValue().getPublisher().getName();
			return new ReadOnlyStringWrapper(publisherName);
		});
		
		
		//inicjalizacja kolumn dla copyTableView
		getSignatureColumn().setCellValueFactory(new PropertyValueFactory<>("signature"));
		getCopyStatusColumn().setCellValueFactory(new PropertyValueFactory<>("status"));
		
		
		//inicjalizacja kolumn dla chosenCopyTableView
		getChosenSignatureColumn().setCellValueFactory(new PropertyValueFactory<>("signature"));
		getChosenTitleColumn().setCellValueFactory(cellData -> {
			String title = cellData.getValue().getProduct().getTitle();
			return new ReadOnlyStringWrapper(title);
		});
		
		
		//załadowanie danych z bazy o produktach do productTableView
		loadDataToProductTableView(getProducts());
		
	}
	
	
	@FXML
	public void findCopy() {
		if(getSignatureTextField().getText().trim().equals("")) {
			setChosenCopy(null);
			setLabelsNullChosenCopy();	
			loadDataToProductTableView(getProducts());
			loadDataToCopyTableView(new HashSet<Copy>());
			return;
		}
		
		CopyDao copyDao = new CopyDao();
		setChosenCopy(copyDao.findBySignature(getSignatureTextField().getText()));
		
		if(getChosenCopy() == null) {
			setLabelsNullChosenCopy();		
			showDialogNullChosenCopy();
			loadDataToCopyTableView(new HashSet<Copy>());
		} else {
			setLabelsOKChosenCopy();
			loadDataToProductTableView(Collections.singletonList(getChosenCopy().getProduct()));
			loadDataToCopyTableView(getChosenCopy().getProduct().getCopyList());
		}

	}
	
	
	@FXML
	public void addCopy() {
		if(getChosenCopy() == null) {
			showDialogNullChosenCopy();
		} else if(isProductChosen(copyList, getChosenCopy().getProduct())) {
			showDialogProductAlreadyChosen();
		} else if (!getChosenCopy().isAvailable()) {
			showDialogNotAvailableChosenCopy();
		} else if(copyList.size() >= 3) {
			showDialogTooManyOrderItems();
		} else {
			getCopyList().add(getChosenCopy());
			System.out.println(getCopyList());
			if(getCopyList().size() == 3) {
				completeOrder();
			}
			loadDataToChosenCopyTableView(getCopyList());
		}
	}
	
	
	@FXML
	public void removeCopy() {
		if(getCopyList().contains(getChosenCopy())) {
			getCopyList().remove(getChosenCopy());
		}
		loadDataToChosenCopyTableView(getCopyList());
	}
	
	
	@FXML
	public void selectProductTableRaw() {
		Product product = getProductTableView().getSelectionModel().getSelectedItem();
		loadDataToCopyTableView(product.getCopyList());
	}
	
	@FXML
	public void cancelOrder() {
		getRegisterOrderController().loadScreen();
	}
	
	
	@FXML
	public void enterPressed(KeyEvent key) {
		System.out.println(key);
		if(key.getCode().equals(KeyCode.ENTER)) {
			findCopy();
		}
	}
	
	@FXML
	public void completeOrder() {
		if(getCopyList().size() == 0) {
			showDialogEmptyCopyList();
		} else {
		
			FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/OrderDetailsWindow.fxml"));
			Pane pane = null;
			
			try {
				pane = loader.load();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			OrderDetailsController orderDetailsController = loader.getController();
			orderDetailsController.setCopyList(getCopyList());
			orderDetailsController.setClient(getClient());
			orderDetailsController.loadDataToOrderDetailsTableView(getCopyList());
			orderDetailsController.loadDataToClientDetailsTableView(Collections.singletonList(getClient()));
			orderDetailsController.setRegisterOrderController(getRegisterOrderController());
			
			getRegisterOrderController().setContainer(pane);
		}
	}
	
	
	
	public void loadDataToProductTableView(List<Product> productList) {
		ObservableList<Product> observableList = FXCollections.observableArrayList(productList);
		productTableView.setItems(observableList);
	}

	
	
	public void loadDataToCopyTableView(Set<Copy> list) {
		ObservableList<Copy> observableList = FXCollections.observableArrayList(list);
		copyTableView.setItems(observableList);
	}
	
	
	public void loadDataToChosenCopyTableView(Set<Copy> list) {
		ObservableList<Copy> observableList = FXCollections.observableArrayList(list);
		chosenCopyTableView.setItems(observableList);
	}

	
	public List<Product> getProducts(){
		ProductDao productDao = new ProductDao();
		return productDao.getAll();
	}
	
	
	public boolean isProductChosen(Set<Copy> copyList, Product product) {
		for(Copy copy : copyList) {
			if(copy.getProduct().equals(product)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	public void setLabelsOKChosenCopy() {
		getSignatureLabel().setText(getChosenCopy().getSignature());
		getCopyStatusLabel().setText(getChosenCopy().getStatus().toString());
	}
	
	
	public void setLabelsNullChosenCopy() {
		getSignatureLabel().setText("[empty]");
		getCopyStatusLabel().setText("[empty]");	
	}
	
	
	
	private void showDialogNullChosenCopy() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Wrong signature error");
		alert.setHeaderText("Copy was not found");
		alert.setContentText("Type proper signature in text box and confirm with Find button.");

		alert.showAndWait();
	}
	
	
	private void showDialogNotAvailableChosenCopy() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Not available");
		alert.setHeaderText("Copy is not availabe");
		alert.setContentText("Only available copies can be rented.");

		alert.showAndWait();
	}
	
	
	private void showDialogProductAlreadyChosen() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Wrong product");
		alert.setHeaderText("Product already chosen");
		alert.setContentText("Only one copy of one product can be rented.");

		alert.showAndWait();
	}
	
	
	private void showDialogTooManyOrderItems() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Wrong amont of copies");
		alert.setHeaderText("Too many copies in order");
		alert.setContentText("Only three copies can be rented in one order.");

		alert.showAndWait();
	}
	
	
	private void showDialogEmptyCopyList() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Empty copy list");
		alert.setHeaderText("Any copy was chosen");
		alert.setContentText("Choose at least one copy to complete order.");

		alert.showAndWait();
	}
	
	
	public TableView<Product> getProductTableView() {
		return productTableView;
	}


	public void setProductTableView(TableView<Product> productTableView) {
		this.productTableView = productTableView;
	}


	public TableColumn<Product, String> getTitleColumn() {
		return titleColumn;
	}


	public void setTitleColumn(TableColumn<Product, String> titleColumn) {
		this.titleColumn = titleColumn;
	}


	public TableColumn<Product, Integer> getEditionColumn() {
		return editionColumn;
	}


	public void setEditionColumn(TableColumn<Product, Integer> editionColumn) {
		this.editionColumn = editionColumn;
	}


	public TableColumn<Product, Integer> getReleaseYearColumn() {
		return releaseYearColumn;
	}


	public void setReleaseYearColumn(TableColumn<Product, Integer> releaseYearColumn) {
		this.releaseYearColumn = releaseYearColumn;
	}


	public TableColumn<Product, String> getAuthorColumn() {
		return authorColumn;
	}


	public void setAuthorColumn(TableColumn<Product, String> authorColumn) {
		this.authorColumn = authorColumn;
	}


	public TableColumn<Product, String> getPublisherColumn() {
		return publisherColumn;
	}


	public void setPublisherColumn(TableColumn<Product, String> publisherColumn) {
		this.publisherColumn = publisherColumn;
	}


	public TableView<Copy> getCopyTableView() {
		return copyTableView;
	}


	public void setCopyTableView(TableView<Copy> copyTableView) {
		this.copyTableView = copyTableView;
	}


	public TableColumn<Copy, String> getSignatureColumn() {
		return signatureColumn;
	}


	public void setSignatureColumn(TableColumn<Copy, String> signatureColumn) {
		this.signatureColumn = signatureColumn;
	}


	public TableColumn<Copy, String> getCopyStatusColumn() {
		return copyStatusColumn;
	}


	public void setCopyStatusColumn(TableColumn<Copy, String> copyStatusColumn) {
		this.copyStatusColumn = copyStatusColumn;
	}


	public TextField getSignatureTextField() {
		return signatureTextField;
	}


	public void setSignatureTextField(TextField signatureTextField) {
		this.signatureTextField = signatureTextField;
	}


	public Label getSignatureLabel() {
		return signatureLabel;
	}


	public void setSignatureLabel(Label signatureLabel) {
		this.signatureLabel = signatureLabel;
	}


	public Label getCopyStatusLabel() {
		return copyStatusLabel;
	}


	public void setCopyStatusLabel(Label copyStatusLabel) {
		this.copyStatusLabel = copyStatusLabel;
	}


	public Copy getChosenCopy() {
		return chosenCopy;
	}


	public void setChosenCopy(Copy chosenCopy) {
		this.chosenCopy = chosenCopy;
	}


	public NaturalPerson getClient() {
		return client;
	}


	public void setClient(NaturalPerson client) {
		this.client = client;
	}


	public Set<Copy> getCopyList() {
		return copyList;
	}


	public void setCopyList(Set<Copy> copyList) {
		this.copyList = copyList;
	}


	public RegisterOrderController getRegisterOrderController() {
		return registerOrderController;
	}


	public void setRegisterOrderController(RegisterOrderController registerOrderController) {
		this.registerOrderController = registerOrderController;
	}


	public TableView<Copy> getChosenCopyTableView() {
		return chosenCopyTableView;
	}


	public void setChosenCopyTableView(TableView<Copy> chosenCopyTableView) {
		this.chosenCopyTableView = chosenCopyTableView;
	}


	public TableColumn<Copy, String> getChosenTitleColumn() {
		return chosenTitleColumn;
	}


	public void setChosenTitleColumn(TableColumn<Copy, String> chosenTitleColumn) {
		this.chosenTitleColumn = chosenTitleColumn;
	}


	public TableColumn<Copy, String> getChosenSignatureColumn() {
		return chosenSignatureColumn;
	}


	public void setChosenSignatureColumn(TableColumn<Copy, String> chosenSignatureColumn) {
		this.chosenSignatureColumn = chosenSignatureColumn;
	}


	
	
	
	
	
	
	
}
