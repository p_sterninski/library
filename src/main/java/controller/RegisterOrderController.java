package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class RegisterOrderController {

	@FXML
	private StackPane registerOrderStackPane;
	
	private Stage stage;
	
	@FXML
	public void initialize() {
		loadScreen();
		
	}
	
	public void loadScreen() {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/ClientChooseWindow.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ClientChooseController clientChooseController = loader.getController();
		clientChooseController.setRegisterOrderController(this);
		setContainer(pane);
	}
	
	public void setContainer(Pane pane) {
		registerOrderStackPane.getChildren().clear();
		registerOrderStackPane.getChildren().add(pane);
	}

	
	public StackPane getRegisterOrderStackPane() {
		return registerOrderStackPane;
	}

	public void setRegisterOrderStackPane(StackPane registerOrderStackPane) {
		this.registerOrderStackPane = registerOrderStackPane;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	
	
	
	
}
