package controller;

import java.util.Collections;
import java.util.List;

import dao.person.NaturalPersonDao;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import model.exception.BibraryEmployeeException;
import model.person.NaturalPerson;

public class ClientChooseController {
	
	@FXML
	private TableView<NaturalPerson> clientTableView;
	
	@FXML
	private TableColumn<NaturalPerson, String> cardNoColumn;
	
	@FXML
	private TableColumn<NaturalPerson, String> firstNameColumn;
	
	@FXML
	private TableColumn<NaturalPerson, String> lastNameColumn;
	
	@FXML
	private TableColumn<NaturalPerson, String> emailColumn;
	
	@FXML
	private TableColumn<NaturalPerson, String> blockedColumn;
	
	@FXML
	private TextField cardNoTextField;
	
	@FXML
	private Label cardNoLabel;
	
	@FXML
	private Label isBlockedLabel;
	
	private RegisterOrderController registerOrderController;
	private NaturalPerson chosenClient;
	
	
	public void initialize() {
		getCardNoColumn().setCellValueFactory(new PropertyValueFactory<>("cardNo"));
		getFirstNameColumn().setCellValueFactory(new PropertyValueFactory<>("firstName"));
		getLastNameColumn().setCellValueFactory(new PropertyValueFactory<>("lastName"));
		getEmailColumn().setCellValueFactory(new PropertyValueFactory<>("email"));
		getBlockedColumn().setCellValueFactory(cellData -> {
			String isBlocked = "";
			try {
				if(cellData.getValue().isBlocked()) {
					isBlocked = "Yes";
				} else {
					isBlocked = "-";
				}
			} catch (BibraryEmployeeException e) {
				isBlocked = "!employee!";
			}
			
			return new ReadOnlyStringWrapper(isBlocked);
		});
		
		loadDataToClientTableView(getClients());
		
	}
	
	@FXML
	public void addClient() {
		try {
			if(getChosenClient() == null) {
				showDialogNullClient();
			
			} else if(getChosenClient().isBlocked()) {
				showDialogBlockedClient();
			
			} else {
			
				FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/OrderItemChooseWindow.fxml"));
				Pane pane = null;
				
				try {
					pane = loader.load();
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				
				
				OrderItemChooseController orderItemChooseController = loader.getController();
				orderItemChooseController.setClient(getChosenClient());
				orderItemChooseController.setRegisterOrderController(getRegisterOrderController());
				getRegisterOrderController().setContainer(pane);
				
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			showDialogNotClient();
		}
	}
	
	
	@FXML
	public void findClient() {
		if(getCardNoTextField().getText().trim().equals("")) {
			setChosenClient(null);
			setLabelsNullClient();	
			loadDataToClientTableView(getClients());
			return;
		}
		
		NaturalPersonDao naturalPersonDao = new NaturalPersonDao();
		setChosenClient(naturalPersonDao.findByCardNo(getCardNoTextField().getText()));
		
		if(getChosenClient() == null) {
			setLabelsNullClient();		
			showDialogNullClient();
		
		} else {
			try {
				setLabelsOKClient();
				loadDataToClientTableView(Collections.singletonList(getChosenClient()));
			} catch (Exception e) {
				setLablesNotClient();
				//e.printStackTrace();
			}
		}
		
	}
	
	@FXML
	public void enterPressed(KeyEvent key) {
		System.out.println(key);
		if(key.getCode().equals(KeyCode.ENTER)) {
			findClient();
		}
	}
	
	
	public void loadDataToClientTableView(List<NaturalPerson> list) {
		ObservableList<NaturalPerson> observableList = FXCollections.observableArrayList(list);
		clientTableView.setItems(observableList);
	}
	
	
	
	public List<NaturalPerson> getClients(){
		NaturalPersonDao naturalPersonDao = new NaturalPersonDao();
		return naturalPersonDao.getClients();	
	}
	
	public void setLabelsOKClient() throws Exception {
		getCardNoLabel().setText(getChosenClient().getCardNo());
		
		String status = "OK";
		if(getChosenClient().isBlocked()) {
			status = "Blocked";
		}
		getIsBlockedLabel().setText(status);
	}
	
	
	public void setLabelsNullClient() {
		getCardNoLabel().setText("[empty]");
		getIsBlockedLabel().setText("[empty]");	
	}
	
	
	public void setLablesNotClient() {
		getCardNoLabel().setText("[empty]");
		getIsBlockedLabel().setText("[empty]");			
	}
	

	
	private void showDialogNullClient() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Wrong client error");
		alert.setHeaderText("Client was not found");
		alert.setContentText("Type proper card number in text box and confirm with Find button.");

		alert.showAndWait();
	}
	
	
	private void showDialogBlockedClient() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Blocked client error");
		alert.setHeaderText("Client is blocked");
		alert.setContentText("Client cannot borrow products when is blocked, check if there are overdue payments.");

		alert.showAndWait();
	}
	
	
	private void showDialogNotClient() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Wrong client error");
		alert.setHeaderText("Person is not client");
		alert.setContentText("Chosen person is not client.");

		alert.showAndWait();
	}
	
	
	public void setRegisterOrderController(RegisterOrderController registerOrderController) {
		this.registerOrderController = registerOrderController;
	}



	public RegisterOrderController getRegisterOrderController() {
		return registerOrderController;
	}

	public TableView<NaturalPerson> getClientTableView() {
		return clientTableView;
	}

	public void setClientTableView(TableView<NaturalPerson> clientTableView) {
		this.clientTableView = clientTableView;
	}

	public TableColumn<NaturalPerson, String> getCardNoColumn() {
		return cardNoColumn;
	}

	public void setCardNoColumn(TableColumn<NaturalPerson, String> cardNoColumn) {
		this.cardNoColumn = cardNoColumn;
	}

	public TableColumn<NaturalPerson, String> getFirstNameColumn() {
		return firstNameColumn;
	}

	public void setFirstNameColumn(TableColumn<NaturalPerson, String> firstNameColumn) {
		this.firstNameColumn = firstNameColumn;
	}

	public TableColumn<NaturalPerson, String> getLastNameColumn() {
		return lastNameColumn;
	}

	public void setLastNameColumn(TableColumn<NaturalPerson, String> lastNameColumn) {
		this.lastNameColumn = lastNameColumn;
	}

	public TableColumn<NaturalPerson, String> getEmailColumn() {
		return emailColumn;
	}

	public void setEmailColumn(TableColumn<NaturalPerson, String> emailColumn) {
		this.emailColumn = emailColumn;
	}

	public TableColumn<NaturalPerson, String> getBlockedColumn() {
		return blockedColumn;
	}

	public void setBlockedColumn(TableColumn<NaturalPerson, String> blockedColumn) {
		this.blockedColumn = blockedColumn;
	}

	public TextField getCardNoTextField() {
		return cardNoTextField;
	}

	public void setCardNoTextField(TextField cardNoTextField) {
		this.cardNoTextField = cardNoTextField;
	}

	public Label getCardNoLabel() {
		return cardNoLabel;
	}

	public void setCardNoLabel(Label cardNoLabel) {
		this.cardNoLabel = cardNoLabel;
	}

	public Label getIsBlockedLabel() {
		return isBlockedLabel;
	}

	public void setIsBlockedLabel(Label isBlockedLabel) {
		this.isBlockedLabel = isBlockedLabel;
	}

	public NaturalPerson getChosenClient() {
		return chosenClient;
	}

	public void setChosenClient(NaturalPerson chosenClient) {
		this.chosenClient = chosenClient;
	}

	
	
	

	
	
}
