package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MenuWindowController {

	@FXML
	public void registerOrder() throws IOException {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/RegisterOrderMainWindow.fxml"));
		Stage primaryStage = new Stage();
		StackPane stackPane = loader.load();
		Scene scene = new Scene(stackPane, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Register order");
		RegisterOrderController registerOrderController = loader.getController();
		registerOrderController.setStage(primaryStage);
		primaryStage.show();
	}
	
	
	@FXML
	public void exitSystem() {
		System.exit(0);
	}
}
