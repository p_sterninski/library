package controller;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import dao.order.OrderDao;
import dao.order.OrderItemDao;
import dao.product.CopyDao;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.InitialData;
import model.order.Order;
import model.order.OrderItem;
import model.person.NaturalPerson;
import model.product.Author;
import model.product.Copy;
import model.product.CopyStatus;

public class OrderDetailsController {

	
	@FXML
	private TableView<NaturalPerson> clientDetailsTableView;

	@FXML
	private TableColumn<NaturalPerson, String> cardNoColumn;
	
	@FXML
	private TableColumn<NaturalPerson, String> firstNameColumn;

	@FXML
	private TableColumn<NaturalPerson, String> lastNameColumn;

	@FXML
	private TableColumn<NaturalPerson, String> emailColumn;
	
	
	
	@FXML
	private TableView<Copy> orderDetailsTableView;

	@FXML
	private TableColumn<Copy, String> signatureColumn;
	
	@FXML
	private TableColumn<Copy, String> titleColumn;

	@FXML
	private TableColumn<Copy, Number> editionColumn;

	@FXML
	private TableColumn<Copy, Number> releaseYearColumn;
	
	@FXML
	private TableColumn<Copy, String> authorColumn;
	
	@FXML
	private TableColumn<Copy, String> publisherColumn;
	
	//private Order pendingOrder;
	private NaturalPerson client;
	private Set<Copy> copyList = new HashSet<>();
	private RegisterOrderController registerOrderController;
	
	
	public void initialize() {
		
		//inicjalizacja kolumn dla clientDetailsTableView
		getCardNoColumn().setCellValueFactory(new PropertyValueFactory<>("cardNo"));
		getFirstNameColumn().setCellValueFactory(new PropertyValueFactory<>("firstName"));
		getLastNameColumn().setCellValueFactory(new PropertyValueFactory<>("lastName"));
		getEmailColumn().setCellValueFactory(new PropertyValueFactory<>("email"));
		
		
		//inicjalizacja kolumn dla orderDetailsTableView
		getSignatureColumn().setCellValueFactory(new PropertyValueFactory<>("signature"));
		
		getTitleColumn().setCellValueFactory(cellData -> {
			String title = cellData.getValue().getProduct().getTitle();
			return new ReadOnlyStringWrapper(title);
		});
		
		getEditionColumn().setCellValueFactory(cellData -> {
			int edition = cellData.getValue().getProduct().getEdition();
			return new ReadOnlyIntegerWrapper(edition);
		});
		
		
		getReleaseYearColumn().setCellValueFactory(cellData -> {
			int releaseYear = cellData.getValue().getProduct().getReleaseYear();
			return new ReadOnlyIntegerWrapper(releaseYear);
		});
		
		getAuthorColumn().setCellValueFactory(cellData -> {
			String authors = cellData.getValue().getProduct()
									.getAuthorList()
									.stream()
									.map(Author::toString)
									.collect(Collectors.joining(", "));
			return new ReadOnlyStringWrapper(authors);
		});
		
		getPublisherColumn().setCellValueFactory(cellData -> {
			String publisherName = cellData.getValue().getProduct().getPublisher().getName();
			return new ReadOnlyStringWrapper(publisherName);
		});
		
		
		//inicjalizacja kolumn dla copyTableView
//		getSignatureColumn().setCellValueFactory(new PropertyValueFactory<>("signature"));
//		getCopyStatusColumn().setCellValueFactory(new PropertyValueFactory<>("status"));
		
		
	}
	
	
	@FXML
	public void acceptOrder() {
		try {
			Order order = new Order(LocalDate.now());
			order.addClient(getClient());
			//order.addEmployee(MainGui.employee);
			//order.addEmployee(Main.employee);
			order.addEmployee(InitialData.employee);
			new OrderDao().save(order);
			
			int clientReturnPeriod = getClient().getReturnPeriod();
			LocalDate returnDateForcast = LocalDate.now().plusDays(clientReturnPeriod);
			for(Copy copy : getCopyList()) {
				OrderItem orderItem = OrderItem.register(order, LocalDate.now(), returnDateForcast, copy);
				new OrderItemDao().save(orderItem);
				copy.setStatus(CopyStatus.RENTED);
				new CopyDao().update(copy);
			}
			getRegisterOrderController().loadScreen();
			//getRegisterOrderController().getStage().close();
		}catch (Exception e) {
			showDialogOrderError();
			e.printStackTrace();
		}
				
	}
	
	
	@FXML
	public void cancelOrder() {
		//getRegisterOrderController().loadScreen();
		getRegisterOrderController().getStage().close();
	}
	
	
	private void showDialogOrderError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Order error");
		alert.setHeaderText("Order cannot be saved");

		alert.showAndWait();
	}
	
	
	public void loadDataToOrderDetailsTableView(Set<Copy> copyList) {
		ObservableList<Copy> observableList = FXCollections.observableArrayList(copyList);
		getOrderDetailsTableView().setItems(observableList);
	}


	public void loadDataToClientDetailsTableView(List<NaturalPerson> list) {
		ObservableList<NaturalPerson> observableList = FXCollections.observableArrayList(list);
		clientDetailsTableView.setItems(observableList);
	}
	
	
	public TableView<NaturalPerson> getClientDetailsTableView() {
		return clientDetailsTableView;
	}


	public void setClientDetailsTableView(TableView<NaturalPerson> clientDetailsTableView) {
		this.clientDetailsTableView = clientDetailsTableView;
	}


	public TableColumn<NaturalPerson, String> getCardNoColumn() {
		return cardNoColumn;
	}


	public void setCardNoColumn(TableColumn<NaturalPerson, String> cardNoColumn) {
		this.cardNoColumn = cardNoColumn;
	}


	public TableColumn<NaturalPerson, String> getFirstNameColumn() {
		return firstNameColumn;
	}


	public void setFirstNameColumn(TableColumn<NaturalPerson, String> firstNameColumn) {
		this.firstNameColumn = firstNameColumn;
	}


	public TableColumn<NaturalPerson, String> getLastNameColumn() {
		return lastNameColumn;
	}


	public void setLastNameColumn(TableColumn<NaturalPerson, String> lastNameColumn) {
		this.lastNameColumn = lastNameColumn;
	}


	public TableColumn<NaturalPerson, String> getEmailColumn() {
		return emailColumn;
	}


	public void setEmailColumn(TableColumn<NaturalPerson, String> emailColumn) {
		this.emailColumn = emailColumn;
	}


	public TableView<Copy> getOrderDetailsTableView() {
		return orderDetailsTableView;
	}


	public void setOrderDetailsTableView(TableView<Copy> orderDetailsTableView) {
		this.orderDetailsTableView = orderDetailsTableView;
	}


	public TableColumn<Copy, String> getTitleColumn() {
		return titleColumn;
	}


	public void setTitleColumn(TableColumn<Copy, String> titleColumn) {
		this.titleColumn = titleColumn;
	}


	public TableColumn<Copy, Number> getEditionColumn() {
		return editionColumn;
	}


	public void setEditionColumn(TableColumn<Copy, Number> editionColumn) {
		this.editionColumn = editionColumn;
	}


	public TableColumn<Copy, Number> getReleaseYearColumn() {
		return releaseYearColumn;
	}


	public void setReleaseYearColumn(TableColumn<Copy, Number> releaseYearColumn) {
		this.releaseYearColumn = releaseYearColumn;
	}


	public TableColumn<Copy, String> getAuthorColumn() {
		return authorColumn;
	}


	public void setAuthorColumn(TableColumn<Copy, String> authorColumn) {
		this.authorColumn = authorColumn;
	}


	public TableColumn<Copy, String> getPublisherColumn() {
		return publisherColumn;
	}


	public void setPublisherColumn(TableColumn<Copy, String> publisherColumn) {
		this.publisherColumn = publisherColumn;
	}


	public NaturalPerson getClient() {
		return client;
	}


	public void setClient(NaturalPerson client) {
		this.client = client;
	}


	public Set<Copy> getCopyList() {
		return copyList;
	}


	public void setCopyList(Set<Copy> copyList) {
		this.copyList = copyList;
	}


	public TableColumn<Copy, String> getSignatureColumn() {
		return signatureColumn;
	}


	public void setSignatureColumn(TableColumn<Copy, String> signatureColumn) {
		this.signatureColumn = signatureColumn;
	}


	public RegisterOrderController getRegisterOrderController() {
		return registerOrderController;
	}


	public void setRegisterOrderController(RegisterOrderController registerOrderController) {
		this.registerOrderController = registerOrderController;
	}


	
	


	
	
	
	
	
	
}
