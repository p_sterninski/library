package model.exception;

/**
 * Wyjątek rzucany w momencie gdy metoda jest wywoływana przez pracownika
 * a dostępna jest tylko dla klienta
 * 
 * @author Przemysław Sterniński
 */
public class BibraryEmployeeException extends Exception {

	/**
	 * Konstruktor
	 * 
	 * @param message wiadomość wyświetlana przy wyjątku
	 */
	public BibraryEmployeeException(String message) {
		super(message);
	}
}
