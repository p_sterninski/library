package model.exception;

/**
 * Wyjątek rzucany w momencie gdy w momencie tworzenia pozycji
 * zamówienia, nie podane zostało zamówienie z nim powiązane
 * 
 * @author Przemysław Sterniński
 */
public class BibraryMissingOrderException extends Exception{

	/**
	 * Konstruktor
	 * 
	 * @param message wiadomość wyświetlana przy wyjątku
	 */
	public BibraryMissingOrderException(String message) {
		super(message);
	}
}
