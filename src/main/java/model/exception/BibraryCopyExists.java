package model.exception;

public class BibraryCopyExists extends Exception {

	public BibraryCopyExists(String message) {
		super(message);
	}
}
