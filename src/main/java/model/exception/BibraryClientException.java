package model.exception;

/**
 * Wyjątek rzucany w momencie gdy metoda jest wywoływana przez klienta
 * a dostępna jest tylko dla pracownika
 * 
 * @author Przemysław Sterniński
 */
public class BibraryClientException extends Exception{
	
	/**
	 * Konstruktor
	 * 
	 * @param message wiadomość wyświetlana przy wyjątku
	 */
	public BibraryClientException(String message) {
		super(message);
	}
}
