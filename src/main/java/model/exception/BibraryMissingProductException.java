package model.exception;

/**
 * Wyjątek rzucany w momencie gdy w momencie tworzenia egzemplarza
 * , nie podany został produkt z nim powiązany
 * 
 * @author Przemysław Sterniński
 */
public class BibraryMissingProductException extends Exception{

	/**
	 * Konstruktor
	 * 
	 * @param message wiadomość wyświetlana przy wyjątku
	 */
	public BibraryMissingProductException(String message) {
		super(message);
	}
}
