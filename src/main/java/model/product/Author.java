package model.product;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Autor praduktu dostepnego w bibliotece.
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Author")
public class Author {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "author_ID")
	private Long id;
	private String firstName;
	private String lastName;
	
	@ManyToMany(mappedBy = "authorList")
	private Set<Product> productList = new HashSet<>();

	
//konstruktory
	/**
	 * konstruktor bezparametrowy
	 */
	public Author() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param firstName	imię autora
	 * @param lastName	nazwisko autora
	 */
	public Author(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}
	
	
//metody
	public String toString() {
		return getFirstName() + " " + getLastName();
	}	
	
	
	
//id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
//firstName
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


//lastName
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
//productList
	public Set<Product> getProductList() {
		return productList;
	}

	public void setProductList(Set<Product> productList) {
		this.productList = productList;
	}
	
	
	
}
