package model.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Jezyk w którym napisany został produkt
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Language")
public class Language {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "language_ID")	
	private Long id;
	private String name;
	
	@OneToMany(mappedBy = "language")
	private List<Product> productList = new ArrayList<>();
	
	
//konstruktory
	/**
	 * Konstruktor bezparametrowy
	 */
	public Language() {}
	
	
	/**
	 * Konstruktor
	 * @param name	nazwa języka
	 */
	public Language(String name) {
		this.setName(name);
	}

	
	
//id
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
//name
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	
//productList
	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	
	
	
	

}
