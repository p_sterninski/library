package model.product;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Dział do którego przypisany został produkt
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table (name = "Section")
public class Section {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "section_ID")	
	private Long id;
	private String name;

	@OneToMany
	@MapKey(name = "productCode")
	private Map<String, Product> productMap = new HashMap<>();

	
	
//konstruktory
	/**
	 * konstruktor bezparametrowy
	 */
	public Section() {}
	
	
	/**
	 * Konstruktor
	 * @param name nazwa działu
	 */
	public Section(String name) {
		setName(name);
	}


//metody
	public Product findProduct(String productCode) throws Exception {
		if(!getProductMap().containsKey(productCode)) {
			throw new Exception("Cannot find product with code: " + productCode);
		}
		
		Product product = getProductMap().get(productCode);
		return product;
	}
	
	
	public String toString() {
		return this.getClass().getSimpleName() + " " +
				"["
				+ "name = " + getName()
				+ "]";				
	}
	
	
	
//Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
	
	
//name
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
//productMap
	public Map<String, Product> getProductMap() {
		return productMap;
	}


	public void setProductMap(Map<String, Product> productMap) {
		this.productMap = productMap;
	}
	
	
}
