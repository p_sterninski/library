package model.product;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.exception.BibraryCopyExists;
import model.exception.BibraryMissingProductException;
import model.order.OrderItem;

/**
 * Egzemplarz danego produktu
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Copy")
public class Copy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id; 
	
	@Column(unique = true)
	private String signature;
	
	@Convert(converter = CopyStatusConverter.class)
	private CopyStatus status;
	
	@Column(nullable = true)
	private LocalDate availableDate;
	
	@Column(nullable = true)
	private LocalDate withdrawnDate;
	
	@ManyToOne
	private Product product;
	
	@OneToMany(mappedBy = "copy", fetch=FetchType.EAGER)
	private List<OrderItem> orderItemList = new ArrayList<>();
	

// konstruktory
	private Copy() {}
	
	private Copy(Product newProduct, String signature) throws BibraryMissingProductException, BibraryCopyExists {
		if(newProduct == null) {
			throw new BibraryMissingProductException("Cannot create Copy without Product");
		}
		newProduct.addCopy(this);
		setProduct(newProduct);
		setSignature(signature);
		setStatus(CopyStatus.UNAVAILABLE);
	}	
	
	
	/**
	 * Metoda tworząca egzemplarz produktu
	 * 
	 * @param newProduct	produkt powiązany z egzempalrzem
	 * @param signature		sygnatura egzemplarza
	 * @return				instancja egzemplarza
	 * @throws BibraryMissingProductException wyjątek rzucany gdy dodany produkt jest null
	 * @throws BibraryCopyExists 
	 */
	public static Copy register(Product newProduct, String signature) throws BibraryMissingProductException, BibraryCopyExists{
		Copy copy = new Copy(newProduct, signature);
		return copy;
	}
	
	
	
//metody klasowe
	/**
	 * Zmienia status egzempalrza na dostępny
	 * @param date	data udostępnienia egzemplarza do wypożyczania
	 */
	public void makeAvailabe(LocalDate date) {
		setAvailableDate(date);
		setStatus(CopyStatus.AVAILABLE);
	}
	
	
	/**
	 * Wycofuje egzempalarz z listy do wypożyczenia
	 * @param date	data wycowania egzempalrza
	 */
	public void withdraw(LocalDate date) {
		setWithdrawnDate(date);
		setStatus(CopyStatus.WITHDRAWN);
	}
	
	
	/**
	 * Metoda 	sprawdzająca czy egzempalrz jest dostępny
	 * @return 	zwraca true jeśli egzemplarz jest dostępny, lub false w przeciwnym przypadku
	 */
	public boolean isAvailable() {
		return getStatus().equals(CopyStatus.AVAILABLE);
	}
	
	
	/**
	 * Zwraca listę pozycji zamówienia dotyczących tego egzemplarza
	 */
	public void showOrderItem() {
		System.out.println(this + " - linked to:");
		for(Object e : getOrderItemList()) {
			System.out.println("\t" + e);
		}
	}
	
	
	public String toString() {
		return this.getClass().getSimpleName() + " " +
				"["
				+ "signature = " + getSignature()
				+ ", status = " + getStatus()
				+ ", availableDate = " + getAvailableDate()
				+ ", withdrawnDate = " + getWithdrawnDate()
				+ "]";
	}
	

//id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


//signature
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	
//status
	public CopyStatus getStatus() {
		return status;
	}
	
	public void setStatus(CopyStatus status) {
		this.status = status;
	}
	

//availableDate
	public LocalDate getAvailableDate() {
		if(availableDate == null) {
			return LocalDate.of(1900, 1, 1);
		} else {
			return availableDate;
		}
	}
	
	public void setAvailableDate(LocalDate availableDate) {
		this.availableDate = availableDate;
	}
	
	
//product	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
//withdrawnDate
	public LocalDate getWithdrawnDate() {
		if(withdrawnDate == null) {
			return LocalDate.of(1900, 1, 1);
		} else {
			return withdrawnDate;
		}
	}

	public void setWithdrawnDate(LocalDate withdrawnDate) {
		this.withdrawnDate = withdrawnDate;
	}


//orderItemList
	public List<OrderItem> getOrderItemList() {
		return orderItemList;
	}


	public void setOrderItemList(List<OrderItem> orderItemList) {
		this.orderItemList = orderItemList;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((availableDate == null) ? 0 : availableDate.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((withdrawnDate == null) ? 0 : withdrawnDate.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Copy other = (Copy) obj;
		if (availableDate == null) {
			if (other.availableDate != null)
				return false;
		} else if (!availableDate.equals(other.availableDate))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (status != other.status)
			return false;
		if (withdrawnDate == null) {
			if (other.withdrawnDate != null)
				return false;
		} else if (!withdrawnDate.equals(other.withdrawnDate))
			return false;
		return true;
	}

	
	
	
	
}