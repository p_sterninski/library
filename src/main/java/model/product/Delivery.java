package model.product;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import model.person.LegalPerson;

/**
 * Szczegóły dostawy produktu
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Delivery")
public class Delivery {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "vendor_product_ID")
	private Long id;
	private int amount;
	private LocalDate deliveryDate;
	
	@Column(nullable = true)
	private Double unitPrice;
	
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_ID") 
	private Product product;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "legal_person_ID") 
	private LegalPerson legalPerson;
	
	

//konstruktory
    /**
     * konstruktor bezparametrowy
     */
    public Delivery() {}
    
    
    /**
     * Konstruktor
     * 
     * @param amount		dostarczona ilość produktu
     * @param unitPrice		cena jednostkowa produktu
     * @param deliveryDate	data dostawy
     * @param legalPerson	osoba prawna (wydawca lub dostawca) od której pozyskano produkt
     * @param product		produkt którego dotyczy dostawa
     */
	public Delivery(int amount, Double unitPrice, LocalDate deliveryDate, LegalPerson legalPerson, Product product) {
		setAmount(amount);
		setUnitPrice(unitPrice);
		setDeliveryDate(deliveryDate);
		addLegalPerson(legalPerson);
		addProduct(product);
	}


//add
	/**
	 * Dodaje osobę prawną do dostawy.
	 * 
	 * @param newLegalPerson	osoba prawna powiązana z dostawą
	 */
	public void addLegalPerson(LegalPerson newLegalPerson) {
		if(getLegalPerson() == null) {
			setLegalPerson(newLegalPerson);
			newLegalPerson.getDeliveryList().add(this);
		}
	}
	
	
	/**
	 * Dodaje produkt, którego dotyczy dostawa.
	 * 
	 * @param newProduct	produkt kórego dotyczy dostawa
	 */
	public void addProduct(Product newProduct) {
		if(getProduct() == null) {
			setProduct(newProduct);
			newProduct.getDeliveryList().add(this);
		}
	}


//metody
	public String toString() {
		return this.getClass().getSimpleName() + " " +
				"["
				+ "amount = " + getAmount()
				+ ", unitPrice = " + getUnitPrice()
				+ ", deliveryDate = " + getDeliveryDate()
				+ "]";	
	}
	
	
	
//amount
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
//deliveryDate
	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}
	
	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	

//unitPrice
	public Double getUnitPrice() {
		if(unitPrice == null) {
			return 0.0;
		}
		
		return unitPrice;
	}
	
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}


//product
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}


//privatePerson
	public LegalPerson getLegalPerson() {
		return legalPerson;
	}

	public void setLegalPerson(LegalPerson legalPerson) {
		this.legalPerson = legalPerson;
	}
	
	
}
