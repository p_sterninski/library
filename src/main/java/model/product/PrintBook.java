package model.product;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import model.person.Publisher;

/**
 * Produkt będący produktem drukowanym
 * 
 * @author Przemysław Sterniński
 *
 */
@Entity
@Table(name = "PrintBook")
@PrimaryKeyJoinColumn(name = "product_ID")
public class PrintBook extends Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "printBook_ID")
	private Long id;
	private String ISBN;
	private int pagesNo;
	
	
//konstruktory
	/**
	 * Konstruktor bezparametrowy
	 */
	public PrintBook() {}
	
	
	/**
	 * Konstruktor 
	 * 
	 * @param productCode				kod książki
	 * @param title						tytył książki
	 * @param edition					numer wydania
	 * @param releaseYear				rok wydania
	 * @param publisher					wydawca książki
	 * @param section					dział w bibliotece, do którego przypisana została książka
	 * @param language					język, w którym napisana została książka
	 * @param polishTitleTranslation	polskie tłumaczenie książki
	 * @param ISBN						ISBN książki
	 * @param noPages					ilość stron książki
	 * @throws Exception				wyjątek rzucony jeśli do polskiej książki dodano także polskie tłumaczenie 
	 * 									lub jeśli do książki zagranicznej nie dodano polskiego tłumaczenia
	 */
	public PrintBook(String productCode, String title, int edition, int releaseYear, Publisher publisher, Section section, Language language, String polishTitleTranslation, String ISBN, int noPages) throws Exception {
		super(productCode, title, edition, releaseYear, publisher, section, language, polishTitleTranslation);
		setISBN(ISBN);
		setPagesNo(noPages);
	}
	
	
	
//	private PrintBook(String code, String title, int edition, int releaseYear, double price, String ISBN, int noPages) throws Exception {
//		super(code, title, edition, releaseYear, price);
//		setISBN(ISBN);
//		setPagesNo(noPages);
//		
//	}
	
	
//register non-polish product
//	public static PrintBook register(String code, String title, int edition, int releaseYear, Publisher publisher, Section section, Language language, String polishTitleTranslation, String ISBN, int noPages)
//			throws Exception{
//		
//		return new PrintBook(code, title, edition, releaseYear, publisher, section, language, polishTitleTranslation, ISBN, noPages);
//	}
	
	
////register polish product
//	public static PrintBook register(String code, String title, int edition, int releaseYear, double price, String ISBN, int noPages)
//			throws Exception{
//		
//		return new PrintBook(code, title, edition, releaseYear, price, ISBN, noPages);
//	}		
	
	
//metody
	public String getDescription() {
		return this.toString();
	}

	
	public String toString() {
		return super.toString() 
				+", ISBN " + getISBN()
				+", " + getPagesNo() + " pages";
	}	
	
	
	
//id
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	//ISBN
	public String getISBN() {
		return ISBN;
	}
	
	
	public void setISBN(String ISBN) throws Exception {
		if(StringUtils.isBlank(ISBN)) {
			throw new Exception("No ISBN inserted");
		}
		
		this.ISBN = ISBN;
	}
	

	
//noPages
	public int getPagesNo() {
		return pagesNo;
	}
	
	
	public void setPagesNo(int pagesNo) throws Exception {
		if(pagesNo <= 0) {
			throw new Exception("Number of pages cannot be less or equal 0");
		}
		
		this.pagesNo = pagesNo;
	}

	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ISBN == null) ? 0 : ISBN.hashCode());
		result = prime * result + pagesNo;
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrintBook other = (PrintBook) obj;
		if (ISBN == null) {
			if (other.ISBN != null)
				return false;
		} else if (!ISBN.equals(other.ISBN))
			return false;
		if (pagesNo != other.pagesNo)
			return false;
		return true;
	}
}
