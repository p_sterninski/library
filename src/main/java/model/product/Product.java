package model.product;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import model.exception.BibraryCopyExists;
import model.person.Publisher;

/**
 * Klasa abstrakcyjna.
 * Opisuje cechy wspólne produktów dostępnych w bibliotece
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Product")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Product 
	//extends Extension implements Serializable 
	{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "product_ID")
	private Long id;
	private String productCode;
	private String title;
	private int edition;
	private int releaseYear;
    
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(
            name = "Product_Author",
            joinColumns = @JoinColumn(name = "product_ID"),
            inverseJoinColumns = @JoinColumn(name = "author_ID"))
	private Set<Author> authorList = new HashSet<>();
	
	@OneToMany(mappedBy = "product", orphanRemoval = true, cascade = CascadeType.PERSIST, fetch=FetchType.EAGER)
	private Set<Copy> copyList = new HashSet<>();
	
	@ManyToOne
	private Language language;
	
	@Column(nullable = true)
	private String polishTitleTranslation;
	
	@OneToMany(mappedBy = "product")
	private Set<Delivery> deliveryList = new HashSet<>();
	
	@ManyToOne
	private Section section;
	
	@ManyToOne
	private Publisher publisher;
	
	@Transient
	private static Set<Copy> allCopies = new HashSet<>();
	
	
	
	
//constructor
	/**
	 * Konstruktor bezparametrowy
	 */
	public Product() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param productCode				kod produktu
	 * @param title						tytył produktu
	 * @param edition					numer wydania
	 * @param releaseYear				rok wydania
	 * @param publisher					wydawca produktu
	 * @param section					dział w bibliotece, do którego przypisany został produkt
	 * @param language					język, w którym napisany został produkt
	 * @param polishTitleTranslation	polskie tłumaczenie produktu
	 * @throws Exception				wyjątek rzucony jeśli do polskiego produktu dodano także polskie tłumaczenie 
	 * 									lub jeśli do produktu zagranicznego nie dodano polskiego tłumaczenia
	 */
	public Product(String productCode, String title, int edition, int releaseYear, Publisher publisher, Section section, Language language, String polishTitleTranslation) throws Exception {
		
		if (!language.getName().toLowerCase().equals("polish") && (polishTitleTranslation == null || polishTitleTranslation.trim().equals(""))) {
			throw new Exception("Non-polish product must have polish translation of a title");
		} else if(language.getName().toLowerCase().equals("polish") && polishTitleTranslation != null && !polishTitleTranslation.trim().equals("")) {
			throw new Exception("Polish product can't have polish translation of a title");
		}
		
		setProductCode(productCode);
		setTitle(title);
		setEdition(edition);
		setReleaseYear(releaseYear);
		setPublisher(publisher);
		addSection(section);
		setPolishTitleTranslation(polishTitleTranslation);
		addLanguage(language);
		
	}
	
	
//	public Product(String code, String title, int edition, int releaseYear) throws Exception {
//		//super();
//		
//		setProductCode(code);
//		setTitle(title);
//		setEdition(edition);
//		setReleaseYear(releaseYear);
//	}
	


//add
	/**
	 * Dodaje dział w biblitece.
	 * 
	 * @param newSection dział książek biblioteki
	 */
	public void addSection(Section newSection) {
		if(getSection() == null) {
			setSection(newSection);
			
			// Dodaj informację zwrotną
			newSection.getProductMap().put(this.getProductCode(), this);
		}
	}

	/**
	 * Dodaje egzemplarz do produktu.
	 * 
	 * @param newCopy				egzemplarz przypisany do produktu
	 * @throws BibraryCopyExists 	wyjątek rzucany w sytuacji gdy egzemplarz ten został już przypisany do innego produktu
	 */
	public void addCopy(Copy newCopy) throws BibraryCopyExists {
		if(!getCopyList().contains(newCopy)) {
			if(allCopies.contains(newCopy)) {
				throw new BibraryCopyExists("That copy is linked to Product");
			}
			getCopyList().add(newCopy);
			getAllCopies().add(newCopy);
		}
	}
	
	
	/**
	 * Dodaje język do produktu
	 * 
	 * @param newLanguage	język, w którym został napisany produkt
	 */
	public void addLanguage(Language newLanguage) {
		if(getLanguage() == null) {
			setLanguage(newLanguage);
			
			// Dodaj informację zwrotną
			newLanguage.getProductList().add(this);
		}		
	}
	
	
	/**
	 * Dodaje wydawcę
	 * 
	 * @param newPublisher	wydawca tego produktu
	 */
	public void addPublihser(Publisher newPublisher) {
		if(getPublisher() == null) {
			setPublisher(newPublisher);
			
			// Dodaj informację zwrotną
			newPublisher.getProductList().add(this);
		}		
	}
	
	
	/**
	 * Dodaje autora produktu
	 * 
	 * @param newAuthor	autor tego produktu
	 */
	public void addAuthor(Author newAuthor) {
		if(!getAuthorList().contains(newAuthor)){
			getAuthorList().add(newAuthor);
		}
		
		// Dodaj informację zwrotną
		newAuthor.getProductList().add(this);
		
	}
	
	
//	public void addDelivery(Delivery newDelivery) {
//		if(!getDeliveryList().contains(newDelivery)) {
//			getDeliveryList().add(newDelivery);
//			newDelivery.addProduct(this);
//		}
//	}
//	
//	
//	public void addDelivery(Delivery newDelivery) {
//		if(!getDeliveryList().contains(newDelivery)) {
//			getDeliveryList().add(newDelivery);
//			newDelivery.addProduct(this);
//		}
//	}
//	
//	
	
	
//	public void delete() {
//		for(Copy c : getCopyList()) {
//			c.deleteProduct(this);
//			this.getCopyList().remove(c);
//		}
//	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	
//code
	public String getProductCode() {
		return productCode;
	}
	
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


//title
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws Exception {
		if(StringUtils.isBlank(title)) {
			throw new Exception("No title inserted");
		}
				
		this.title = title;
	}

	
//edition
	public int getEdition() {
		return edition;
	}

	public void setEdition(int edition) throws Exception {
		if(edition < 1) {
			throw new Exception("Edition cannot be less than 1");			
		}
		
		this.edition = edition;
	}

	
//releaseYear	
	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}



////amountAvailable
//	public int getAmountAvailable() {
//		int amountAvailable = 0;
//		for(Copy copy : getCopyList()) {
//			if(copy.getStatus() == CopyStatus.AVAILABLE) {
//				++amountAvailable;
//			}
//		}
//		
//		return amountAvailable;
//		
//	}

	
////authorList
//	public List<Author> getAuthorList() {
//		return authorList;
//	}
//
//	public void setAuthorList(List<Author> authorList) {
//		this.authorList = authorList;
//	}
//
//	
//copyList
	public Set<Copy> getCopyList() {
		return copyList;
	}

	public void setCopyList(Set<Copy> copyList) {
		this.copyList = copyList;
	}
	
	
	
//allCopies
	public static Set<Copy> getAllCopies() {
		return allCopies;
	}


	public static void setAllCopies(Set<Copy> allCopies) {
		Product.allCopies = allCopies;
	}



//language
	public Language getLanguage() {
		return language;
	}


	public void setLanguage(Language language) {
		this.language = language;
	}


//polishTitle
	public String getPolishTitleTranslation() throws Exception {
		if(getLanguage() != null && getLanguage().getName().toLowerCase().equals("polish")) {
			throw new Exception("Polish title translation do not exists for polish product");
		}
		
		return polishTitleTranslation;
	}


	public void setPolishTitleTranslation(String polishTitleTranslation) throws Exception {
		if(getLanguage() != null && getLanguage().getName().toLowerCase().equals("polish")) {
			throw new Exception("Polish title translation do not exists for polish product");
		}
		this.polishTitleTranslation = polishTitleTranslation;
	}
	
	
//deliveryList
	public Set<Delivery> getDeliveryList() {
		return deliveryList;
	}


	public void setDeliveryList(Set<Delivery> deliveryList) {
		this.deliveryList = deliveryList;
	}


//section
	public Section getSection() {
		return section;
	}


	public void setSection(Section section) {
		this.section = section;
	}
	

	
	
//
//
////genreList
//	public List<Genre> getGenreList() {
//		return genreList;
//	}
//
//
//	public void setGenreList(List<Genre> genreList) {
//		this.genreList = genreList;
//	}
//
//
	

//publisher
	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	
//authorList
	public Set<Author> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(Set<Author> authorList) {
		this.authorList = authorList;
	}

	
//showCopy
	public void showCopy() {
		System.out.println(this + " - linked to:");
		for(Object e : getCopyList()) {
			System.out.println("\t" + e);
		}
	}
	
	
//showDelivery
	public void showDelivery() {
		System.out.println(this + " - linked to:");
		for(Object e : getDeliveryList()) {
			System.out.println("\t" + e);
		}
	}
	

//getShortDescription
	public String getDescription() {
		return this.toString();
	}
	
	

//toString
	public String toString() {
		return this.getClass().getSimpleName()
				+ ", id " + getId()
				+ ", productCode " + getProductCode()
				+ ": " + getTitle()
				+ ", edition " + getEdition()
				+ ", released " + getReleaseYear();
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + edition;
		result = prime * result + ((polishTitleTranslation == null) ? 0 : polishTitleTranslation.hashCode());
		result = prime * result + releaseYear;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (edition != other.edition)
			return false;
		if (polishTitleTranslation == null) {
			if (other.polishTitleTranslation != null)
				return false;
		} else if (!polishTitleTranslation.equals(other.polishTitleTranslation))
			return false;
		if (releaseYear != other.releaseYear)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
}