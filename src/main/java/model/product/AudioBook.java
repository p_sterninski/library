package model.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import model.person.Publisher;

/**
 * Produkt będący audiobook'iem
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "AudioBook")
@PrimaryKeyJoinColumn(name = "product_ID")
public class AudioBook extends Product{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "audioBook_ID")
	private Long id;
	private int duration; //in minutes
	
	
	/**
	 * Konstruktor bezparametrowy
	 */
	public AudioBook() {}
	
	
	/**
	 * 
	 * @param productCode				kod audiobooka
	 * @param title						tytył audiobooka
	 * @param edition					numer wydania audiobooka
	 * @param releaseYear				rok wydania audiobooka
	 * @param publisher					wydawca audiobooka
	 * @param section					dział w bibliotece, do którego przypisany został audiobook
	 * @param language					język, w którym napisany został audiobook
	 * @param polishTitleTranslation	polskie tłumaczenie audiobooka
	 * @param duration					czas trwania nagrania w minutach
	 * @throws Exception				wyjątek rzucony jeśli do polskiego audiobooka dodano także polskie tłumaczenie 
	 * 									lub jeśli do audiobooka zagranicznego nie dodano polskiego tłumaczenia
	 */
	public AudioBook(String productCode,String title, int edition, int releaseYear, Publisher publisher, Section section, Language language, String polishTitleTranslation, int duration) throws Exception {
		super(productCode, title, edition, releaseYear, publisher, section, language, polishTitleTranslation);
		setDuration(duration);
		
	}
	

	
//metody
	public String getDescription() {
		return this.toString();
	}
	
	
	public String toString() {
		return super.toString() 
				+", " + getDuration() + " min";
	}
	
	
	
		
//id
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}

	
//duration
	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}	

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + duration;
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AudioBook other = (AudioBook) obj;
		if (duration != other.duration)
			return false;
		return true;
	}
	
	
	
	
}
