package model.product;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CopyStatusConverter implements AttributeConverter<CopyStatus, Character> {
    
	@Override
    public Character convertToDatabaseColumn(CopyStatus copyStatus) {
        return copyStatus.getShortName();
    }
 
    @Override
    public CopyStatus convertToEntityAttribute(Character dbData) {
        return CopyStatus.fromShortName(dbData);
    }
}
