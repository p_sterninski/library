package model.product;

public enum CopyStatus {
	UNAVAILABLE('U'),
	AVAILABLE('A'),
	RENTED('R'),
	WITHDRAWN('W');
	
	
	
    private char shortName;
	 
    private CopyStatus(char shortName) {
        this.shortName = shortName;
    }
 
    public char getShortName() {
        return shortName;
    }
 
    public static CopyStatus fromShortName(char shortName) {
        switch (shortName) {
        case 'A':
            return CopyStatus.AVAILABLE;
 
        case 'U':
            return CopyStatus.UNAVAILABLE;
 
        case 'R':
            return CopyStatus.RENTED;
 
        case 'W':
            return CopyStatus.WITHDRAWN;
 
        default:
            throw new IllegalArgumentException("ShortName [" + shortName
                    + "] not supported.");
        }
    }
}
