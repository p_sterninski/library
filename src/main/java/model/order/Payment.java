package model.order;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import model.exception.BibraryEmployeeException;
import model.person.NaturalPerson;
import model.person.PersonStatus;

/**
 * Płatność wynikająca z nie oddania egzempalrza w terminie
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Payment")
public class Payment {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "payment_ID")
	private Long id;
	private LocalDate paymentDate;
	private double amount;
	private static double rate = 2;
	
	@ManyToOne
	private NaturalPerson client;
	
	@OneToOne
	private OrderItem orderItem;
	
	
	
//konstruktor
	/**
	 * Konstruktor bezparametrowy
	 */
	public Payment() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param paymentDate	data dokonania płatności
	 * @param amount		kwota płatności
	 */
	public Payment(LocalDate paymentDate, double amount) {
		setPaymentDate(paymentDate);
		setAmount(amount);
	}

	
//add
	/**
	 * Metoda doająca klienta do płatności
	 * 
	 * @param newClient	klient, którego dotyczy płatnoś
	 * @throws BibraryEmployeeException	wyjątek rzucany gdy klient jest tylko pracownikiem
	 */
	public void addClient(NaturalPerson newClient) throws BibraryEmployeeException {
		if(newClient.getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee cannot make a payment");
		}
		else if(getClient() == null) {
			this.setClient(newClient);
			
			// informacja zwrotna
			newClient.getPaymentList().add(this);
		}
	}
	
	
	/**
	 * Metoda doająca powązaną pozycję zamówienia
	 * 
	 * @param orderItem	pozycja zamówienia która nie została oddana w terminie
	 */
	public void addOrderItem(OrderItem orderItem) {
		if(getOrderItem() == null) {
			setOrderItem(orderItem);
			
			// informacja zwrotna
			orderItem.setPayment(this);
		}
	}
	
	
//paymentDate
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	
//amount
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}


//client
	public NaturalPerson getClient() {
		return client;
	}

	public void setClient(NaturalPerson client) {
		this.client = client;
	}


//orderItem
	public OrderItem getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}



//rate
	public static double getRate() {
		return rate;
	}

	public static void setRate(double rate) {
		Payment.rate = rate;
	}
	
	
	
	
}
