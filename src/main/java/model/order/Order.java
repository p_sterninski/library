package model.order;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.exception.BibraryClientException;
import model.exception.BibraryEmployeeException;
import model.person.NaturalPerson;
import model.person.PersonStatus;
import model.product.Product;


/**
 * Zamówienie składane przez klienta, przechowujące maksymalnie
 * trzy pozycje zamówienia, w których każde musi być innego produktu
 * 
 * @author Przemysław Sterniński
 */

@Entity
@Table(name = "Orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "order_ID")
	private Long id;
	private LocalDate openDate;
	
	@Column(nullable = true)
	private LocalDate closeDate;
	
	@OneToMany(mappedBy = "order", orphanRemoval = true, cascade = CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<OrderItem> orderItemList = new ArrayList<>();
	
	@ManyToOne
	private NaturalPerson client;
	
	@ManyToOne
	private NaturalPerson employee;
	

//konstruktor
	/**
	 * Konstruktor bezparametrowy
	 */
	public Order() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param openDate data utworzenia zamowienia
	 */
	public Order(LocalDate openDate) {
		setOpenDate(openDate);
	}
	
	
//add
	/**
	 * Metoda dodająca osobę składającą zamówienia zamówenia
	 * 
	 * @param 	newClient klient który ma zostać dodany do zamówienia
	 * @throws 	BibraryEmployeeException wyjątek rzucany gdy do zamówienia dodawany jest pracownik
	 */
	public void addClient(NaturalPerson newClient) throws BibraryEmployeeException {
		if(newClient.getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee cannot make an order");
		}
		else if(getClient() == null) {
			this.setClient(newClient);
			
			// Dodaj informację zwrotną
			newClient.getMadeOrderList().add(this);
		}
	}
	
	
	/**
	 * Metoda dodająca pracownika rejestrującego zamówienie
	 * 
	 * @param newEmployee pracownik który ma zostać dodany do zamówienia
	 * @throws BibraryClientException wyjątek rzucany gdy do zamówienia dodawany jest klient
	 */
	public void addEmployee(NaturalPerson newEmployee) throws BibraryClientException {
		if(newEmployee.getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client cannot register an order");
		}
		if(getEmployee() == null) {
			this.setEmployee(newEmployee);
			
			// Dodaj informację zwrotną(this);
			newEmployee.getRegisteredOrderList().add(this);
		}
	}
	

	
//metody
	public boolean isProductInOrder(Product product) {
		for(OrderItem orderItem : getOrderItemList()) {
			System.out.println(orderItem.getCopy().getProduct() + " - " + product);
			if(orderItem.getCopy().getProduct().equals(product)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	
	public int countOrderitems() {
		return getOrderItemList().size();
	}
	
	
	
//id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


//openDate
	public LocalDate getOpenDate() {
		return openDate;
	}
	
	public void setOpenDate(LocalDate openDate) {
		this.openDate = openDate;
	}
	
	
//closeDate
	public LocalDate getCloseDate() {
		if(closeDate == null) {
			return LocalDate.of(2200, 1, 1);
		} else {
			return closeDate;
		}
	}
	
	public void setCloseDate(LocalDate closeDate) {
		this.closeDate = closeDate;
	}
	

//orderItemList
	public List<OrderItem> getOrderItemList() {
		return orderItemList;
	}

	public void setOrderItemList(List<OrderItem> orderItemList) {
		this.orderItemList = orderItemList;
	}


//client
	public NaturalPerson getClient() {
		return client;
	}

	public void setClient(NaturalPerson client) {
		this.client = client;
	}


//employee
	public NaturalPerson getEmployee() {
		return employee;
	}


	public void setEmployee(NaturalPerson employee) {
		this.employee = employee;
	}	
	
	
	
	
	
	
}
