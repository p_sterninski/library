package model.order;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import model.exception.BibraryClientException;
import model.exception.BibraryMissingOrderException;
import model.person.NaturalPerson;
import model.person.PersonStatus;
import model.product.Copy;

/**
 * 
 * Pozycja zamówienia zawierająca informacje o wypożyczonym egzemplarzu
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Order_Item")
public class OrderItem {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "order_item_ID")
	private Long id;
	private LocalDate rentDate;
	private LocalDate returnDateForecast;
	
	@Column(nullable = true)
	private LocalDate returnDate;
	
	@ManyToOne
	private Copy copy;
	
	@ManyToOne
	private Order order;
	
	@ManyToOne
	private NaturalPerson employee;
	
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn(name="payment_id")
	private Payment payment;
	

	
//konstruktory
	private OrderItem() {}
	
	
	private OrderItem(Order order, LocalDate rentDate, LocalDate returnDateForecast, Copy copy) throws BibraryMissingOrderException {
		if(order == null) {
			throw new BibraryMissingOrderException("Cannot create OrderItem without Order");
		}
		setRentDate(rentDate);
		setReturnDateForecast(returnDateForecast);
		setOrder(order);
		getOrder().getOrderItemList().add(this);
		addCopy(copy);
	}
	
	
	/**
	 * Metoda statyczna tworząca instancję pozycji zamówienia
	 * 
	 * @param order 				zamówienie powiązane z pozycją zamówienia
	 * @param rentDate				data wypożyczenia egzemplarza
	 * @param returnDateForecast	data kiedy egzemplarz ma zostać zwrócony
	 * @param copy					egzempalrz którego dotyczy pozycja zamówienia
	 * @throws BibraryMissingOrderException	wyjątek rzucany gdy dodane zamówienie jest null
	 */
	public static OrderItem register(Order order, LocalDate rentDate, LocalDate returnDateForecast, Copy copy) throws Exception {
		OrderItem orderItem = new OrderItem(order, rentDate, returnDateForecast, copy);
		return orderItem;
	}
	
	
	
//add
	/**
	 * Metoda dodająca egzemplarz do pozycji zamówienia
	 * 
	 * @param newCopy egzemplarz którego dotyczy pozycja zamówienia
	 */
	public void addCopy(Copy newCopy) {
		if(getCopy() == null) {
			setCopy(newCopy);
			newCopy.getOrderItemList().add(this);
		}
	}
	
	
	public void addEmployee(NaturalPerson employee) throws BibraryClientException {
		if(employee.getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client cannot close an order item");
		}
		if(getEmployee() == null) {
			setEmployee(employee);
			
			//informacja zwrotna
			employee.getClosedOrderItemList().add(this);
		}
	}

	
	
//metody
	public String toString() {
		return this.getClass().getSimpleName() + " " +
				"["
				+ "rental date = " + getRentDate()
				+ "]";
	}	
	
	
	
//id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
//rentalDate
	public LocalDate getRentDate() {
		return rentDate;
	}

	public void setRentDate(LocalDate rentDate) {
		this.rentDate = rentDate;
	}
	
	
//returnDateForecast
	public LocalDate getReturnDateForecast() {
		return returnDateForecast;
	}
	
	public void setReturnDateForecast(LocalDate returnDateForecast) {
		this.returnDateForecast = returnDateForecast;
	}
	
	
//returnDate
	public LocalDate getReturnDate() {
		if(returnDate == null) {
			return LocalDate.of(1900, 1, 1);
		} else {
			return returnDate;
		}
	}
	
	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}


//copy
	public Copy getCopy() {
		return copy;
	}


	public void setCopy(Copy copy) {
		this.copy = copy;
	}
	

//order
	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}
	
	
//payment
	public Payment getPayment() {
		return payment;
	}


	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	
//employee
	public NaturalPerson getEmployee() {
		return employee;
	}

	public void setEmployee(NaturalPerson employee) {
		this.employee = employee;
	}

	
	
	
	
}
