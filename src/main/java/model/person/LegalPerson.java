package model.person;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.product.Delivery;

/**
 * Klasa abstrakcyjna.
 * Osabą prawną może być dostawca lub wydawca
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "Legal_Person")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="is_publisher", discriminatorType=DiscriminatorType.STRING)
public abstract class LegalPerson {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "legal_person_ID")
	private Long id;
	private String name;
	private String NIP;
	

	@OneToMany(mappedBy = "legalPerson")
	private List<Delivery> deliveryList = new ArrayList<>();
	
	
//constructor
	/**
	 * konstruktor bezparametrowy
	 */
	public LegalPerson() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param name	nazwa osoby prywatnej
	 * @param NIP	NIP osoby prywatnej
	 */
	public LegalPerson(String name, String NIP) {
		setName(name);
		setNIP(NIP);
	}


	
//id
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}


//name
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

//NIP
	public String getNIP() {
		return NIP;
	}
	
	public void setNIP(String NIP) {
		this.NIP = NIP;
	}


//deliveryList
	public List<Delivery> getDeliveryList() {
		return deliveryList;
	}

	public void setDeliveryList(List<Delivery> deliveryList) {
		this.deliveryList = deliveryList;
	}
	
	
}
