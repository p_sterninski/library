package model.person;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Adres klienta i/lub pracownika
 * 
 * @author Przemysław Sterniński
 *
 */

@Embeddable
public class Address {
	private String street;
	private int houseNo;
	@Column(nullable = true)
	private Integer flatNo;
	private String zipCode;
	private String city;
	
	
//konstruktory
	/**
	 * Konstruktor bezparametrowy
	 */
	public Address() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param street	ulica
	 * @param houseNo	numer domu
	 * @param flatNo	numer mieszkania
	 * @param zipCode	kod pocztowy
	 * @param city		miasto
	 */
	public Address(String street, Integer houseNo, Integer flatNo, String zipCode, String city) {
		super();
		setStreet(street);
		setHouseNo(houseNo);
		setFlatNo(flatNo);
		setZipCode(zipCode);
		setCity(city);
	}
	
	
//street
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	
//houseNo
	public int getHouseNo() {
		return houseNo;
	}
	
	public void setHouseNo(int houseNo) {
		this.houseNo = houseNo;
	}
	
	
//flatNo
	public Integer getFlatNo() {
		if(flatNo == null) {
			return 0;
		}
		return flatNo;
	}
	
	public void setFlatNo(Integer flatNo) {
		this.flatNo = flatNo;
	}

	
//city
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}


//zipCode
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
}
