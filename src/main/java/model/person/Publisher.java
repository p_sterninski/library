package model.person;

/**
 * Osoba prawna będąca wydawcą produktów w bibliotece
 * 
 * @author Przemysław Sterniński
 */

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.product.Product;

@Entity
@Table(name = "Publisher")
@DiscriminatorValue(value="Y")
public class Publisher extends LegalPerson {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "publisher_ID")
	private Long id;
	
	@OneToMany(mappedBy = "publisher")
	private List<Product> productList = new ArrayList<>();
	
	
//konstruktory
	/**
	 * konstruktor bezparametrowy
	 */
	public Publisher() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param name	nazwa wydawcy
	 * @param NIP	NIP wydawcy
	 */
	public Publisher(String name, String NIP) {
		super(name, NIP);
	}
	
	
	
//id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
//productList
	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	
	
	
	
}
