package model.person;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class PersonStatusConverter implements AttributeConverter<PersonStatus, String> {
	
	@Override
    public String convertToDatabaseColumn(PersonStatus personStatus) {
        return personStatus.getShortName();
    }
 
    @Override
    public PersonStatus convertToEntityAttribute(String dbData) {
        return PersonStatus.fromShortName(dbData);
    }
}
