package model.person;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Osoba prawna będąca dostawca produktów biblioteki
 * 
 * @author Przemysław Sterniński
 *
 */
@Entity
@Table(name = "Publisher")
@DiscriminatorValue(value="N")
public class Vendor extends LegalPerson {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "vendor_ID")
	private Long id;
	

//konstruktory
	/**
	 * konstruktor bezparametrowy
	 */
	public Vendor() {}
	
	
	/**
	 * Konstruktor
	 * 
	 * @param name	imię dostawcy
	 * @param NIP	NIP dostawcy
	 */
	public Vendor(String name, String NIP) {
		super(name, NIP);
	}
	
	
	
//id
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
}
