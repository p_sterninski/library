package model.person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.exception.BibraryClientException;
import model.exception.BibraryEmployeeException;
import model.order.Order;
import model.order.OrderItem;
import model.order.Payment;

/**
 * Osobą fizyczną może być pracownik biblioteki lub jej klient
 * 
 * @author Przemysław Sterniński
 *
 */

@Entity
@Table(name = "NaturalPerson")
public class NaturalPerson {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	
	@Embedded
	private Address address; //@Embedded oznacza że adres w bazie znajduje się w tabeli NaturalPerson (nie ma tabeli dla adresu)
	
	@Convert(converter = PersonStatusConverter.class)
	private PersonStatus personStatus;
	
//Employee
	@Column(nullable = true)
	private LocalDate employmentDate;
	
	@Column(nullable = true)
	private LocalDate dismissDate;
	
	@OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
	private List<Order> registeredOrderList = new ArrayList<>();
	
	@OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
	private List<OrderItem> closedOrderItemList = new ArrayList<>();
	
//Client
	private static int returnPeriodInEducation = 21;
	private static int returnPeriodAfterEducation = 14;
	
	@Column(nullable = true)
	private String cardNo;
	
	@Column(nullable = true)
	private LocalDate registerDate;
	
	@Column(nullable = true)
	private Boolean isBlocked;
	
	@Column(nullable = true)
	private EducationStatus clientEducationStatus;
	
	@OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
	private List<Order> madeOrderList = new ArrayList<>();
	
	@OneToMany(mappedBy = "client")
	private List<Payment> paymentList = new ArrayList<>();
	
	

//konstruktory
	private NaturalPerson() {}
	

	private NaturalPerson(String firstName, String lastName, String email,
			PersonStatus status, LocalDate employmentDate, String cardNo,
			LocalDate registerDate, Boolean isBlocked, EducationStatus clientEducationStatus) throws BibraryClientException, BibraryEmployeeException {
		
		setId(id);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setEmploymentDate(employmentDate);
		setDismissDate(dismissDate);
		setCardNo(cardNo);
		setRegisterDate(registerDate);
		setIsBlocked(isBlocked);
		setClientEducationStatus(clientEducationStatus);
		setPersonStatus(status);
	}
	
	
	/**
	 * Metoda tworząca klienta
	 * 
	 * @param firstName				imię klienta
	 * @param lastName				nazwisko klienta
	 * @param email					e-mail klienta
	 * @param cardNo				numer karty klienta
	 * @param registerDate			data zarejestrowania klienta
	 * @param clientEducationStatus	status określający czy klient jest w trakcie nauki czy nie
	 * @return						zwraca osobę fizyczną będącą klientem
	 * @throws Exception			wyjatek rzucany gdy osoba jest pracownikiem
	 */
	public static NaturalPerson registerClient(String firstName, String lastName, String email, String cardNo, LocalDate registerDate, EducationStatus clientEducationStatus) throws Exception  {
		return new NaturalPerson(firstName, lastName, email, PersonStatus.Client, null, cardNo, registerDate, false, clientEducationStatus);
	}
	
	
	/**
	 * Metoda tworząca pracownika
	 * 
	 * @param firstName			imię pracownika
	 * @param lastName			nazwisko pracownika
	 * @param email				e-mail pracownika
	 * @param employmentDate	data zatrudnienia pracownika
	 * @return					osoba fizyczna będąca pracownikiem biblioteki
	 * @throws Exception		wyjątek rzucany gdy osoba jest klientem
	 */
	public static NaturalPerson registerEmployee(String firstName, String lastName, String email, LocalDate employmentDate) throws Exception {
		return new NaturalPerson(firstName, lastName, email, PersonStatus.Employee, employmentDate, null, null, null, null);
	}
	
	
	/**
	 * Metoda tworząca osobą będacą zarazem pracwnikiem i klientem
	 * 
	 * @param firstName				imię osoby
	 * @param lastName				nazwisko osoby
	 * @param email					e-mail osoby
	 * @param employmentDate		data zatrudnienia jako pracownik
	 * @param cardNo				numer karty jako klient
	 * @param registerDate			data zarejestrowania jako klient
	 * @param clientEducationStatus	status określający czy klient jest w trakcie edukacji czy nie
	 * @return						osoba będąca pracownikiem i klientem biblioteki
	 * @throws Exception
	 */
	public static NaturalPerson registerClientEmployee(String firstName, String lastName, String email, LocalDate employmentDate, String cardNo, LocalDate registerDate, EducationStatus clientEducationStatus) throws Exception {
		return new NaturalPerson(firstName, lastName, email, PersonStatus.Client_Employee, employmentDate, cardNo, registerDate, true, clientEducationStatus);
	}
	

//add
	/**
	 * Metoda dodająca adres osoby fizycznej
	 * 
	 * @param newAddress	adress osoby fizycznej
	 */
	public void addAddress(Address newAddress) {
		if(getAddress() == null) {
			setAddress(newAddress);
		}
	}
	
	
	
//metody
	double sumPayments(LocalDate from, LocalDate to) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have paymentList attribute");
		}
		
		double amount = 0;
		for(Payment payment : getPaymentList()) {
			LocalDate paymentDate = payment.getPaymentDate();
			if(paymentDate.isAfter(from.minusDays(1)) && paymentDate.isBefore(to.plusDays(1))) {
				amount += payment.getAmount();
			}
		}
		
		return amount;
	}
	
	
//g
//id
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
//firstName
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
//lastName
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
//address
	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}


	
	
//email
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}


	
	
//status
	public PersonStatus getPersonStatus() {
		return personStatus;
	}
	
	public void setPersonStatus(PersonStatus personStatus) {
		this.personStatus = personStatus;
	}
	
	
	
//Employee	
	
//employmentDate
	public LocalDate getEmploymentDate() throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have EmploymentDate attribute");
		}
		return employmentDate;
	}
	
	public void setEmploymentDate(LocalDate employmentDate) throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have EmploymentDate attribute");
		}
		this.employmentDate = employmentDate;
	}
	
	

//dismissDate
	public LocalDate getDismissDate() throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have DismissDate attribute");
		}
		return dismissDate;
	}
	
	public void setDismissDate(LocalDate dismissDate) throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have DismissDate attribute");
		}
		this.dismissDate = dismissDate;
	}
	
	
	
//registeredOrderList
	public List<Order> getRegisteredOrderList() throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have RegisteredOrderList attribute");
		}
		return registeredOrderList;
	}


	public void setRegisteredOrderList(List<Order> registeredOrderList) throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have RegisteredOrderList attribute");
		}
		this.registeredOrderList = registeredOrderList;
	}


//closedOrderItemList
	public List<OrderItem> getClosedOrderItemList() throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have closedOrderItemList attribute");
		}
		return closedOrderItemList;
	}

	public void setClosedOrderItemList(List<OrderItem> closedOrderItemList) throws BibraryClientException {
		if(getPersonStatus() == PersonStatus.Client) {
			throw new BibraryClientException("Client does not have closedOrderItemList attribute");
		}
		this.closedOrderItemList = closedOrderItemList;
	}

	
//Client

	//cardNo
	public String getCardNo() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have CardNo attribute");
		}
		return cardNo;
	}
	
	public void setCardNo(String cardNo) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have CardNo attribute");
		}
		this.cardNo = cardNo;
	}
	
	
//registerDate
	public LocalDate getRegisterDate() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have RegisterDate attribute");
		}
		return registerDate;
	}
	
	public void setRegisterDate(LocalDate registerDate) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have RegisterDate attribute");
		}
		this.registerDate = registerDate;
	}
	
	
//isBlocked
	public boolean isBlocked() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have isBlocked attribute");
		}
		return isBlocked;
	}
	
	public void setIsBlocked(Boolean isBlocked) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have isBlocked attribute");
		}
		this.isBlocked = isBlocked;
	}
	
	
//madeOrderList
	public List<Order> getMadeOrderList() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have madeOrderList attribute");
		}
		return madeOrderList;
	}


	public void setMadeOrderList(List<Order> madeOrderList) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have madeOrderList attribute");
		}
		this.madeOrderList = madeOrderList;
	}

	
	
//paymentList
	public List<Payment> getPaymentList() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have paymentList attribute");
		}
		return paymentList;
	}


	public void setPaymentList(List<Payment> paymentList) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have paymentList attribute");
		}
		this.paymentList = paymentList;
	}



	
//clientEducationStatus
	public EducationStatus getClientEducationStatus() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have clientEducationStatus attribute");
		}
		return clientEducationStatus;
	}

	public void setClientEducationStatus(EducationStatus clientEducationStatus) throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have clientEducationStatus attribute");
		}
		this.clientEducationStatus = clientEducationStatus;
	}



//returnPeriod
	public int getReturnPeriod() throws BibraryEmployeeException {
		if(getPersonStatus() == PersonStatus.Employee) {
			throw new BibraryEmployeeException("Employee does not have returnPeriod attribute");
		}
		if(clientEducationStatus == EducationStatus.IN_EDUCATION) {
			return returnPeriodInEducation;
		} else {
			return returnPeriodAfterEducation;
		}
	}
	
	
	
	public static int getReturnPeriod(EducationStatus status) {
		if(status == EducationStatus.IN_EDUCATION) {
			return returnPeriodInEducation;
		} else {
			return returnPeriodAfterEducation;
		}
	}
	
	
}
