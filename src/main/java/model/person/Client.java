package model.person;

import java.time.LocalDate;

public abstract class Client {
	
	private String cardId;
	private LocalDate registryDate;
	
	
	
	
public Client(String cardId, LocalDate registryDate) {
		setCardId(cardId);
		setRegistryDate(registryDate);
	}


//cardId
	public String getCardId() {
		return cardId;
	}
	
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	
//registryDate
	public LocalDate getRegistryDate() {
		return registryDate;
	}
	
	public void setRegistryDate(LocalDate registryDate) {
		this.registryDate = registryDate;
	}
	
	
	
	

}
