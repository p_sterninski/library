package model.person;

public enum PersonStatus {
	Client ("C")
	, Employee ("E")
	, Client_Employee ("A");
	
	private String shortName;
	
	
	private PersonStatus(String shortName) {
        this.shortName = shortName;
    }
 
	
    public String getShortName() {
        return shortName;
    }
    
    
    public static PersonStatus fromShortName(String shortName) {
        switch (shortName) {
        case "C":
            return PersonStatus.Client;
 
        case "E":
            return PersonStatus.Employee;
 
        case "A":
            return PersonStatus.Client_Employee;
 
        default:
            throw new IllegalArgumentException("ShortName [" + shortName
                    + "] not supported.");
        }
    }
}
