//package main;
//
//import java.time.LocalDate;
//
//import org.hibernate.Session;
//
//import dao.HibernateUtil;
//import model.order.OrderItem;
//import model.person.Address;
//import model.person.EducationStatus;
//import model.person.LegalPerson;
//import model.person.NaturalPerson;
//import model.product.AudioBook;
//import model.product.Copy;
//import model.product.CopyStatus;
//import model.product.Delivery;
//import model.product.Language;
//import model.product.PrintBook;
//import model.product.Product;
//import model.product.Section;
//
//public class Test2 {
//	
//	public static Session session;
//	
//	public static void main(String[] args) {
//		
//		session = HibernateUtil.getSessionFactory().getCurrentSession();
//		
//		try {
//			//session = HibernateUtil.getSessionFactory().getCurrentSession();
//	        
//	        session.beginTransaction();
//	        
//	        
//	        System.out.println("\n\n----kompozycja----");
//	        Product product1 = PrintBook.register("AA", "Tytul_AA", 1, 2015, 29.90, "1235", 600);
//	        Copy copy1 = Copy.register(product1, "1111");					//egzemplarz nie może istnieć bez Produktu, asocjacja w metodzie
//	        Copy copy2 = Copy.register(product1, "3333");					//egzemplarz nie może istnieć bez Produktu, asocjacja w metodzie
//	        copy1.setStatus(CopyStatus.AVAILABLE);
//	        copy2.setStatus(CopyStatus.WITHDRAWN);
//	        
//  
//	        //utworzenie nowego produktu i przypisanie już przypisanego do innego produktu egzemplarza
//	        Language languageEng = new Language("english");
//	        Product product2 = AudioBook.register("BB", "Tytul_BB", 1, 2018, 59.90, languageEng, "polishTranslation", 120);
//	        try {
//	        	product2.addCopy(copy2);
//	        	product1.showCopy();
//	        }catch(Exception e) {
//	        	System.out.println("\n" + e);
//	        }
//	        
//	        System.out.println("\n\n----polimorfizm metod----");
//	        System.out.println(product1.getDescription());
//	        System.out.println(product2.getDescription());
//	        //usunięcie produktu
//	        //product1.showCopy();
//
//   
//	        
//	        
//	        System.out.println("\n\n----dziedziczenie overlapping----");
//	        NaturalPerson nPerson1 = NaturalPerson.registerClient("Przemek", "Jaki", "asdf", "sdfg", "afgsd", LocalDate.of(2018, 4, 23), EducationStatus.IN_EDUCATION);
//	        NaturalPerson nPerson2 = NaturalPerson.registerEmployee("Jarek", "Taki", "fdhsdfh", "35347", LocalDate.of(2017, 12, 3));
//
//
//	        System.out.println(nPerson1.getFirstName() + " " + nPerson1.getLastName() + " " + nPerson1.getStatus() + " " + nPerson1.getClientEducationStatus());
//	        System.out.println(nPerson2.getFirstName() + " " + nPerson2.getLastName() + " " + nPerson2.getStatus());
//	        
//	        System.out.println("\n\n----dziedziczenie dynamic----");
//	        System.out.println("\n----Po zmianie statusu edukacji klienta----");
//	        nPerson1.setClientEducationStatus(EducationStatus.AFTER_EDUCATION);
//	        System.out.println(nPerson1.getFirstName() + " " + nPerson1.getLastName() + " " + nPerson1.getStatus() + " " + nPerson1.getClientEducationStatus());
//	        
//	        
//	        
//	        
////	        System.out.println("OK2");
////	        copy1.setProduct(product1);
////	        System.out.println("OK3");
////	        product1.getCopyList().add(copy1);
////	        System.out.println("OK4");
////	        orderItem1.setCopy(copy1);
////	        System.out.println("OK5");
////	        copy1.getOrderItemList().add(orderItem1);
////	        System.out.println("OK6");
////	        product1.setSection(section1);
////	        System.out.println("OK7");	        
////	        //section1.getProductList().add(product1);
////	        section1.getProductMap().put(product1.getCode(), product1);
////	        System.out.println("OK7");	        
//	        
//	        
//	        session.save(languageEng);
//	        session.save(copy1);
//	        session.save(copy2);
//	        session.save(product1);
//	        session.save(product2);
//	        session.save(nPerson1);
//	        session.save(nPerson2);
//	        
//	        
//	        session.getTransaction().commit();
//	        HibernateUtil.getSessionFactory().close();
//	        
//		} catch(Exception sqlException) {
//			System.out.println(sqlException);
//			if(null != session.getTransaction()) {
//                System.out.println("\n.......Transaction Is Being Rolled Back.......");
//                session.getTransaction().rollback();				
//			}
//		} finally {
//			if(session != null) {
//				session.close();
//			}
//		}
//	}
//
//}
