package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

	//public static NaturalPerson employee;
	
	public static void main(String[] args) {
		new InitialData().loadData();
		//employee = new NaturalPersonDao().findById(new Long(4));
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/MenuWindow.fxml"));
		Pane pane = loader.load();
		Scene scene = new Scene(pane, 160, 300);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Menu");
		primaryStage.show();
		
	}

}
