package main;

import java.time.LocalDate;

import org.hibernate.Session;

import dao.HibernateUtil;
import model.order.Order;
import model.order.OrderItem;
import model.person.Address;
import model.person.EducationStatus;
import model.person.NaturalPerson;
import model.person.Publisher;
import model.person.Vendor;
import model.product.AudioBook;
import model.product.Author;
import model.product.Copy;
import model.product.CopyStatus;
import model.product.Delivery;
import model.product.Language;
import model.product.PrintBook;
import model.product.Section;

public class InitialData {

	
	
	public static NaturalPerson employee;
	
	public void loadData() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			session.beginTransaction();
			
			
			Address address1 = new Address("ul. Gorczewska", 3, 17, "01-100", "Warszawa");
			Address address2 = new Address("ul. Domaniewska", 1, 52, "01-121", "Warszawa");
			Address address3 = new Address("ul. Banacha", 5, 3, "01-143", "Warszawa");
			Address address4 = new Address("ul. Leszno", 7, 10, "01-113", "Warszawa");
			
			NaturalPerson client1 = NaturalPerson.registerClient("Jan", "Nowocinski", "j.nowak@poczta.pl", "1234", LocalDate.of(2018, 4, 1), EducationStatus.IN_EDUCATION);
			NaturalPerson client2 = NaturalPerson.registerClient("Krzysztof", "Kowalski", "k.kowalski@poczta.pl", "5436", LocalDate.of(2017, 12, 1), EducationStatus.IN_EDUCATION);
			NaturalPerson client3 = NaturalPerson.registerClient("Agata", "Wisniewska", "a.wisniewska@poczta.pl", "9364", LocalDate.of(2016, 10, 1), EducationStatus.AFTER_EDUCATION);
			
			client1.addAddress(address1);
			client2.addAddress(address2);
			client3.addAddress(address3);
			
			client3.setIsBlocked(true);
			
			NaturalPerson employee1 = NaturalPerson.registerEmployee("Tomasz", "Malewicz", "emp@poczta.pl", LocalDate.of(2017, 2, 1));
			NaturalPerson employee2 = NaturalPerson.registerEmployee("Anna", "Malewicz", "a.malew@gmail.com", LocalDate.of(2015, 10, 1));
			
			employee1.addAddress(address4);
			employee2.addAddress(address3);
			
	        Section fantasySection = new Section("Fantasy");
	        Section horrorSection = new Section("Horror");
	        
	        Language polishLanguage = new Language("polish");
	        Language englishLanguage = new Language("english");
	        
	        Author author1 = new Author("Anna", "Nowak");
	        Author author2 = new Author("Piotr", "Nowak");
	        Author author3 = new Author("Agnieszka", "Malinowska");
	        Author author4 = new Author("Kamil", "Kowalski");
	        Author author5 = new Author("Jan", "Robak");
	        Author author6 = new Author("Monika", "Jeziorska");
	        
	        Publisher publisher1 = new Publisher("ABC Publisher", "12356");
	        Publisher publisher2 = new Publisher("XYZ CopyPrint", "87634");
	        Publisher publisher3 = new Publisher("123 BookCompany", "86245");
	        
			PrintBook printBook = new PrintBook("P01", "tytul AAA1", 1, 2017, publisher1, fantasySection, polishLanguage, null, "999-123-134", 254);
			PrintBook printBook2 = new PrintBook("P02", "tytul AAA2", 9, 2013, publisher2, horrorSection, englishLanguage, "polski tytul AAA2", "999-543-253", 576);
			PrintBook printBook3 = new PrintBook("P03", "tytul AAA3", 3, 2012, publisher3, fantasySection, polishLanguage, null, "999-543-253", 123);
			PrintBook printBook4 = new PrintBook("P04", "tytul AAA4", 4, 2014, publisher2, horrorSection, englishLanguage, "polski tytul AAA4", "999-543-253", 642);
			
			AudioBook audioBook = new AudioBook("A01", "title BBB1", 2, 2015, publisher1, fantasySection, englishLanguage, "tytul BBB1", 120);
			AudioBook audioBook2 = new AudioBook("A02", "title BBB2", 4, 2016, publisher3, horrorSection, polishLanguage, null, 340);
	        
			printBook.addAuthor(author1);
			printBook.addAuthor(author2);
			printBook2.addAuthor(author3);
			printBook3.addAuthor(author6);
			printBook4.addAuthor(author1);
			printBook4.addAuthor(author5);
			audioBook.addAuthor(author2);
			audioBook.addAuthor(author4);
			audioBook2.addAuthor(author5);
			
			
			
			Copy copyPrint11 = Copy.register(printBook, "P0101");
			Copy copyPrint12 = Copy.register(printBook, "P0102");
			Copy copyPrint13 = Copy.register(printBook, "P0103");
			
			Copy copyPrint21 = Copy.register(printBook2, "P0201");
			Copy copyPrint22 = Copy.register(printBook2, "P0202");
			
			Copy copyPrint31 = Copy.register(printBook3, "P0301");
			Copy copyPrint32 = Copy.register(printBook3, "P0302");
			Copy copyPrint33 = Copy.register(printBook3, "P0303");
			Copy copyPrint34 = Copy.register(printBook3, "P0304");
			
			Copy copyPrint41 = Copy.register(printBook4, "P0401");
			Copy copyPrint42 = Copy.register(printBook4, "P0402");
			
			Copy copyAudio11 = Copy.register(audioBook, "A0101");
			Copy copyAudio12 = Copy.register(audioBook, "A0102");
			Copy copyAudio13 = Copy.register(audioBook, "A0103");
			
			Copy copyAudio21 = Copy.register(audioBook2, "A0201");
			Copy copyAudio22 = Copy.register(audioBook2, "A0202");
			
			copyPrint11.makeAvailabe(LocalDate.of(2018, 4, 28));
			copyPrint13.makeAvailabe(LocalDate.of(2018, 4, 28));
			
			copyPrint21.makeAvailabe(LocalDate.of(2018, 4, 28));
			copyPrint22.makeAvailabe(LocalDate.of(2018, 4, 28));

			copyPrint31.makeAvailabe(LocalDate.of(2018, 4, 28));
			copyPrint32.makeAvailabe(LocalDate.of(2018, 4, 28));
			copyPrint34.makeAvailabe(LocalDate.of(2018, 4, 28));
			
			copyPrint41.makeAvailabe(LocalDate.of(2018, 4, 28));
			
			copyAudio11.makeAvailabe(LocalDate.of(2018, 4, 28));
			copyAudio21.makeAvailabe(LocalDate.of(2018, 4, 28));
			
			copyPrint33.setStatus(CopyStatus.WITHDRAWN);
			//copyAudio.makeAvailabe(LocalDate.of(2018, 5, 7));
			
			
			
			Order order1 = new Order(LocalDate.of(2018, 6, 15));
			OrderItem orderItemPrint1 = OrderItem.register(order1, LocalDate.of(2018, 6, 10), LocalDate.of(2018, 6, 24), copyPrint12);
			OrderItem orderItemAudio1 = OrderItem.register(order1, LocalDate.of(2018, 6, 10), LocalDate.of(2018, 6, 24), copyAudio12);
			copyPrint12.setStatus(CopyStatus.RENTED);
			copyAudio12.setStatus(CopyStatus.RENTED);
			order1.addClient(client1);
			order1.addEmployee(employee1);
			
			Order order2 = new Order(LocalDate.of(2018, 6, 15));
			OrderItem orderItemPrint2 = OrderItem.register(order2, LocalDate.of(2018, 5, 10), LocalDate.of(2018, 5, 24), copyPrint22);
			OrderItem orderItemPrint3 = OrderItem.register(order2, LocalDate.of(2018, 5, 10), LocalDate.of(2018, 5, 24), copyPrint34);			
			copyPrint22.setStatus(CopyStatus.RENTED);
			copyPrint34.setStatus(CopyStatus.RENTED);
			order2.addClient(client2);
			order2.addEmployee(employee2);
			
			
			Vendor vendor1 = new Vendor("Vendor ABC", "1234");
	        Delivery vendorDelivery1 = new Delivery(5, 29.90, LocalDate.of(2018, 4, 17), vendor1, printBook);
	        Delivery publisherDelivery1 = new Delivery(7, null, LocalDate.of(2018, 1, 31), publisher1, printBook2);
	        Delivery vendorDelivery2 = new Delivery(15, 34.90, LocalDate.of(2018, 5, 21), vendor1, printBook3);
	        Delivery publisherDelivery2 = new Delivery(25, 9.90, LocalDate.of(2018, 2, 28), publisher1, printBook4);
			
			
			session.save(client1);
			session.save(client2);
			session.save(client3);
			
			session.save(employee1);
			session.save(employee2);
			
			session.save(fantasySection);
			session.save(horrorSection);
			
			session.save(polishLanguage);
			session.save(englishLanguage);
	        
			session.save(author1);
			session.save(author2);
			session.save(author3);
			session.save(author4);
			session.save(author5);
			
			session.save(publisher1);
			session.save(publisher2);
			session.save(publisher3);
			
			session.save(copyPrint11);
			session.save(copyPrint12);
			session.save(copyPrint13);
			
			session.save(copyPrint21);
			session.save(copyPrint22);
			
			session.save(copyPrint31);
			session.save(copyPrint32);
			session.save(copyPrint33);
			session.save(copyPrint34);
			
			session.save(copyPrint41);
			session.save(copyPrint42);
			
			session.save(copyAudio11);
			session.save(copyAudio12);	
			session.save(copyAudio13);	
			
			session.save(copyAudio21);
			session.save(copyAudio22);
			
			session.save(printBook);
			session.save(printBook2);
			session.save(printBook3);
			session.save(printBook4);
			session.save(audioBook);
			session.save(audioBook2);			
			
			session.save(orderItemPrint1);
			session.save(orderItemAudio1);
			session.save(order1);
			
			session.save(orderItemPrint2);
			session.save(orderItemPrint3);
			session.save(order2);
			
			session.save(vendor1);
			session.save(vendorDelivery1);
			session.save(publisherDelivery1);
			session.save(vendorDelivery2);
			session.save(publisherDelivery2);			
			
			
			employee = employee1;
			
	        session.getTransaction().commit();
	        //HibernateUtil.getSessionFactory().close();
		
		} catch(Exception sqlException) {
			System.out.println(sqlException);
			sqlException.printStackTrace();
			if(null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                session.getTransaction().rollback();				
			}
		} finally {
			if(session != null) {
				session.close();
			}
		}
		
	}
}
