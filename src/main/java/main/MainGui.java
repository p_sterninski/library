package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MainGui extends Application {

	//public static NaturalPerson employee;
	
	public static void main(String[] args) {
		new InitialData().loadData();
		//employee = new NaturalPersonDao().findById(new Long(4));
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/gui/RegisterOrderMainWindow.fxml"));
		StackPane stackPane = loader.load();
		Scene scene = new Scene(stackPane, 700, 600);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Register order");
		primaryStage.show();
		
	}

}
